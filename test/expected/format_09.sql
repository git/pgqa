SELECT * FROM (SELECT * FROM b WHERE i =
10) AS s(x, y) JOIN (SELECT * FROM c
WHERE j = 1) AS s2 ON i = j

SELECT  *
FROM    (SELECT *
        FROM    b
        WHERE   i = 10) AS s(x, y) JOIN
        (SELECT *
        FROM    c
        WHERE   j = 1) AS s2 ON i = j

SELECT  *
FROM    (SELECT *
        FROM    b
        WHERE   i = 10) AS s(x, y)
        JOIN
        (SELECT *
        FROM    c
        WHERE   j = 1) AS s2 ON i = j

SELECT  *
FROM    (SELECT *
        FROM    b
        WHERE   i = 10) AS s(x, y)
        JOIN
        (SELECT *
        FROM    c
        WHERE   j = 1) AS s2 ON i = j

SELECT  *
FROM    (SELECT *
        FROM    b
        WHERE
                        i
                =
                        10) AS s(x, y)
        JOIN
        (SELECT *
        FROM    c
        WHERE
                        j
                =
                        1) AS s2 ON
                        i
                =
                        j

SELECT
        *
FROM
        (SELECT
                *
        FROM
                b
        WHERE
                i = 10) AS s(x, y)
        JOIN
        (SELECT
                *
        FROM
                c
        WHERE
                j = 1) AS s2 ON i = j

SELECT
        *
FROM
        (SELECT
                *
        FROM
                b
        WHERE
                        i
                =
                        10) AS s(x, y)
        JOIN
        (SELECT
                *
        FROM
                c
        WHERE
                        j
                =
                        1) AS s2 ON
                        i
                =
                        j

SELECT
    *
FROM
    (SELECT
        *
    FROM
        b
    WHERE
            i
        =
            10) AS s(x, y)
    JOIN
    (SELECT
        *
    FROM
        c
    WHERE
            j
        =
            1) AS s2 ON
            i
        =
            j

SELECT  *
FROM    (SELECT *
        FROM    b
        WHERE   i =
                10)
        AS s(x, y)
        JOIN
        (SELECT *
        FROM    c
        WHERE   j =
                1)
        AS s2 ON i =
            j

SELECT
        *
FROM
        (SELECT
                *
        FROM
                b
        WHERE
                i = 10) AS s(x, y)
        JOIN
        (SELECT
                *
        FROM
                c
        WHERE
                j = 1) AS s2 ON i = j

SELECT
        *
FROM
        (SELECT
                *
        FROM
                b
        WHERE
                        i
                =
                        10) AS s(x, y)
        JOIN
        (SELECT
                *
        FROM
                c
        WHERE
                        j
                =
                        1) AS s2 ON
                        i
                =
                        j

                SELECT * FROM (SELECT * FROM b WHERE i = 10) AS s(x, y) JOIN (SELECT
                * FROM c WHERE j = 1) AS s2 ON i = j

                SELECT  *
                FROM    (SELECT *
                        FROM    b
                        WHERE   i = 10) AS s(x, y) JOIN
                        (SELECT *
                        FROM    c
                        WHERE   j = 1) AS s2 ON i = j

                SELECT
                        *
                FROM
                        (SELECT
                                *
                        FROM
                                b
                        WHERE
                                        i
                                =
                                        10) AS s(x, y)
                        JOIN
                        (SELECT
                                *
                        FROM
                                c
                        WHERE
                                        j
                                =
                                        1) AS s2 ON
                                        i
                                =
                                        j