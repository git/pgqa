SELECT          a.i, avg(b.j)
FROM            a
                JOIN
                (SELECT j
                FROM    b
                WHERE   i = j) AS s1 ON true
                JOIN
                (SELECT x
                FROM    y) AS s2 ON s1.x = s2.i
GROUP BY        a.i
HAVING          avg(b.j) > 1
ORDER BY        a.i

