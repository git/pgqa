# Copyright (C) 2016 Antonin Houska
#
# This file is part of PGQA.
#
# PGQA is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# PGQA is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.

# You should have received a copy of the GNU General Public License qalong
# with PGQA. If not, see <http://www.gnu.org/licenses/>.

VERSION = 0.1

EMACSLOADPATH = :./lisp/pgqa/
INPUT_DIR = ./test/input/
OUTPUT_DIR = ./test/output/
CHECK_DIR = ./test/expected/

TESTS_PARSE = parse_01 parse_02 parse_03

TESTS_FORMAT = format_01 format_02 format_03 format_04 format_05 format_06\
format_07 format_08 format_09 format_10

.PHONY: test
test:
	mkdir -p $(OUTPUT_DIR)

	for TEST_NAME in $(TESTS_PARSE); do \
\
		echo $$TEST_NAME; \
\
		emacs -batch -L $(EMACSLOADPATH) -l pgqa.el \
--insert $(INPUT_DIR)/$$TEST_NAME.sql -f pgqa-parse-query-batch > \
./$(OUTPUT_DIR)/$$TEST_NAME.out \ || exit; \
\
		diff -s $(OUTPUT_DIR)/$$TEST_NAME.out \
$(CHECK_DIR)/$$TEST_NAME.out || exit; \
	done

	for TEST_NAME in $(TESTS_FORMAT); do \
\
		echo $$TEST_NAME; \
\
		emacs -batch -L $(EMACSLOADPATH) -l pgqa-test.el \
--insert $(INPUT_DIR)/$$TEST_NAME.sql -f pgqa-test-formatting > \
./$(OUTPUT_DIR)/$$TEST_NAME.sql \ || exit; \
\
		diff -s $(OUTPUT_DIR)/$$TEST_NAME.sql \
$(CHECK_DIR)/$$TEST_NAME.sql || exit; \
	done

.PHONY: dist
dist:
	rm -rf dist
	mkdir dist
	cp -r lisp/pgqa dist/pgqa-$(VERSION)
	cd dist/pgqa-$(VERSION) && rm -f *.elc && rm -f *~
	cd dist && m4 -DVERSION=$(VERSION) pgqa-$(VERSION)/pgqa-pkg.el.in > \
pgqa-$(VERSION)/pgqa-pkg.el && rm pgqa-$(VERSION)/pgqa-pkg.el.in
	cd dist && tar -H gnu -cf pgqa-$(VERSION).tar pgqa-$(VERSION)

clean:
	rm -rf $(OUTPUT_DIR) dist
