;; Copyright (C) 2016 Antonin Houska
;;
;; This file is part of PGQA.
;;
;; PGQA is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; PGQA is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License qalong
;; with PGQA. If not, see <http://www.gnu.org/licenses/>.

(eval-when-compile
  (require 'pgqa-node-api))

;; Keep byte compiler quitet.
(defvar pgqa-class-field-name)
(defvar pgqa-class-field-parent)
(defvar pgqa-class-field-fields)
(defvar pgqa-class-field-defaults)
(defvar pgqa-class-field-methods)
(defvar pgqa-class-method-dump-raw)
(defvar pgqa-class-method-dump-node)
(defvar pgqa-class-method-walk)
(defvar pgqa-class-method-analyze)
(defvar pgqa-object-field-class)
(defvar pgqa-object-field-fields)
(defvar pgqa-deparse-state)

(defcustom pgqa-multiline-query t
  "If non-nil, query will be deparsed in structured format that spans multiple
lines."
  :type 'boolean
  :group 'pgqa
)

(defcustom pgqa-multiline-join t
  "If non-nil, the JOIN operator (including the INNER, LEFT, etc. predicate)
will always start on a new line.

Can only be set if `pgqa-multiline-query' variable is set."
  :type 'boolean
  :group 'pgqa
)

(defcustom pgqa-join-newline t
  "If non-nil, the tables joined (i.e. not only the JOIN operator) start on a
new line.

If either side of the join requires parentheses (i.e. it's a sub-query or a
join), it starts on a new line regardless this setting.

Can only be set if `pgqa-multiline-join' variable is set."
  :type 'boolean
  :group 'pgqa
)

(defcustom pgqa-join-on-newline nil
  "If non-nil, the ON clause of JOIN is printed on a new line. Extra indentation
is given to the ON clause relative to the JOIN.

Can only be set if `pgqa-join-newline' is set."
  :type 'boolean
  :group 'pgqa)

(defcustom pgqa-clause-newline nil
  "If non-nil, each query clause starts on a new line

Can only be set if `pgqa-multiline-query' variable is set."
  :type 'boolean
  :group 'pgqa
)

(defcustom pgqa-clause-item-newline nil
  "If non-nil, each item of query clause starts on a new line

If the item requires parentheses (i.e. it's a sub-query or a join), this
setting is always considered t.

Can only be set if `pgqa-clause-newline' variable is set."
  :type 'boolean
  :group 'pgqa
)

(defcustom pgqa-multiline-operator nil
  "If non-nil, operators will be deparsed in a structured way.

Can only be set if both `pgqa-multiline-query' and `pgqa-multiline-join'
variables are set."
  :type 'boolean
  :group 'pgqa
)

(defcustom pgqa-multiline-case nil
  "If non-nil, each branch of the CASE expression will start on a new line.

Can only be set if both `pgqa-multiline-query' and `pgqa-multiline-join'
variables are set."
  :type 'boolean
  :group 'pgqa
)

(defcustom pgqa-multiline-case-branch nil
  "If non-nil, THEN keyword of the CASE expression will start on a new line.

Can only be set if `pgqa-multiline-case'is set."
  :type 'boolean
  :group 'pgqa
)

(defcustom pgqa-clause-keyword-right nil
  "If non-nil, the clause keyword (SELECT, FROM, WHERE, etc.) will be right
aligned

Can only be set if `pgqa-multiline-query' is set."
  :type 'boolean
  :group 'pgqa)

(defcustom pgqa-print-as t
  "If non-nil, expression alias is denoted by the AS keyword."

  :type 'boolean
  :group 'pgqa)

;; TODO Rename so that it's formatting-specific.
(defun pgqa-check-customizations ()
  "Throw `user-error' if value of any custom variable is illegal."

  (if pgqa-multiline-join
      (if (null pgqa-multiline-query)
	  (user-error "`pgqa-multiline-join' requires `pgqa-multiline-query'")))

  (if pgqa-join-newline
      (if (null pgqa-multiline-join)
	  (user-error "`pgqa-join-newline' requires `pgqa-multiline-join'")))

  (if pgqa-join-on-newline
      (if (null pgqa-multiline-join)
	  (user-error "`pgqa-join-on-newline' requires `pgqa-multiline-join'")))

  (if pgqa-multiline-case-branch
      (if (null pgqa-multiline-case)
	  (user-error "`pgqa-multiline-case-branch' requires `pgqa-multiline-case'")))

  (if pgqa-multiline-operator
      ;; If the JOIN keyword or the THEN keyword of the CASE expression
      ;; followed an operator expression in the ON clause which is printed out
      ;; in the multiline form, it would become incorrectly indented.
      ;;
      ;; XXX pgqa-multiline-query is redundant here now, but I'm not sure if
      ;; this dependency should be removed. If pgqa-multiline-join became
      ;; unnecessary for pgqa-multiline-operator someday, pgqa-multiline-query
      ;; would have to be added again.
      (if (or (null pgqa-multiline-query) (null pgqa-multiline-join)
	      (null pgqa-multiline-case-branch))
	  (user-error "`pgqa-multiline-operator' requires all `pgqa-multiline-query' and `pgqa-multiline-join' and `pgqa-multiline-case-branch'")))

  (if pgqa-multiline-case
      ;; If the JOIN keyword followed a CASE expression in the ON clausewhich
      ;; is printed out in the multiline form, it would become incorrectly
      ;; indented.
      ;;
      ;; XXX Likewise, pgqa-multiline-query is redundant here.
      (if (or (null pgqa-multiline-query) (null pgqa-multiline-join))
	  (user-error "`pgqa-multiline-case' requires both `pgqa-multiline-query' and `pgqa-multiline-join'")))

  (if pgqa-clause-newline
      (if (null pgqa-multiline-query)
	  (user-error "`pgqa-clause-newline' requires `pgqa-multiline-query'")))

  (if pgqa-clause-item-newline
      (if (null pgqa-clause-newline)
	  (user-error "`pgqa-clause-item-newline' requires `pgqa-clause-newline'")))

  (if pgqa-clause-keyword-right
      (if (null pgqa-multiline-query)
	  (user-error "`pgqa-clause-keyword-right' requires `pgqa-multiline-query'")))
)

;; The entry point to call pgqa-dump-node-... object method.
(defun pgqa-dump (node state indent context)
  (let* ((class (aref node pgqa-object-field-class))
	 (methods (aref class pgqa-class-field-methods))
	 (method (aref methods pgqa-class-method-dump-node)))
    (funcall method node state indent context))
  )

;; If buffer-pos is maintained, use the following functions to adjust the
;; start and end position of the node.
(defun pgqa-dump-start (node state)
  (if (nref state buffer-pos)
      ;; Temporarily set the slot to plain number.
      (nset node region (nref state buffer-pos))))

;; The :gui slot can store either pgqa-gui-node or a list of these. This is a
;; convenience routine so that caller does not have to care.
(defun pgqa-dump-start-gui (node state)
  (if (listp node)
      (dolist (i node)
	(pgqa-dump-start i state))
    (pgqa-dump-start node state))
  )

(defun pgqa-dump-end (node state)
  (if (nref state buffer-pos)
      ;; Retrieve the start position stored by pgqa-dump-start and replace it
      ;; with 2-element vector.
      (let* ((start (nref node region))
	     (end (nref state buffer-pos))
	     (region (vector start end)))
	(nset node region region)))
  )

;; Counterpart of pgqa-dump-start-gui.
(defun pgqa-dump-end-gui (node state)
  (if (listp node)
      (dolist (i node)
	(pgqa-dump-end i state))
    (pgqa-dump-end node state))
  )

;; `state' is an instance of `pgqa-deparse-state' class.
;;
;; `indent' determines indentation of the node, relative to (nref node
;; indent).
;;
;; `context' can affect the way node is dumped depending on the surrounding
;; nodes.
(defun pgqa-dump-node (node state indent &optional context)
  "Turn a node and its children into string."
  nil)

;; Wrapper function that either calls pgqa-dump or prints out SQL insertion if
;; the node is contained in it.
(defun pgqa-dump-maybe-insertion (node state indent context)
  (let ((insertion)
	(insertion-prev)
	(insertion-key)
	(omit))

    (when context
      (setq insertion (nref node insertion))
      (cl-assert (eq (pgqa-object-class-name context)
		     'string-to-query-context))
      (setq insertion-prev (nref context last-insertion))

      ;; Prepare the context for the next node.
      (nset context last-insertion insertion)

      (when insertion
	;; If multiple nodes belong to the same insertion, only print the
	;; insertion string once.
	(when (or (null insertion-prev)
		  (null
		   (string=
		    (nref insertion-prev key)
		    (nref insertion key))))
	  (setq insertion-key (nref insertion key))

	  ;; Insert the insertion. Do it in multiple steps so that line can be
	  ;; broken wherever fill-column is reached.
	  (pgqa-deparse-string state "'" indent)
	  (pgqa-deparse-string state "||" indent)
	  (pgqa-deparse-string state insertion-key indent)
	  (pgqa-deparse-string state "||" indent)
	  (pgqa-deparse-string state "'" indent))

	;; Do not dump the contained node itself.
	(setq omit t))
      )

    (when (null omit)
      (if (eq (pgqa-object-class-name node) 'query)
	  (pgqa-dump-subquery node state indent context)
	(pgqa-dump node state indent context)))
    )
  )

;; init-col-src is column (in addition to the indentation) at which the source
;; query starts.
(defun pgqa-init-deparse-state (indent init-col-src line-empty buffer-pos)
  "Initialize instance of `pgqa-deparse-state'."

  (let ((indent-width (* indent tab-width)))
    (make-instance pgqa-deparse-state
		   :indent indent
		   :indent-top-expr 0
		   :next-column (+ indent-width init-col-src)
		   :next-space 0
		   :line-empty t
		   :buffer-pos buffer-pos
		   :result (make-string indent-width 32))))

(defun pgqa-deparse-state-get-attrs (state)
  "Return the slots of `pgqa-deparse-state' as an association list."
  (let ((result)
	(item))
    (dolist (key (pgqa-object-fields state) result)
      (setq item (list key (pgqa-field-value state key)))
      (push item result))))

(defun pgqa-deparse-state-set-attrs (state fields)
  "Set fields of `pgqa-deparse-state' to values extracted by
`pgqa-deparse-state-get-attrs'."
  (dolist (field fields)
    (pgqa-set-field-value state (car field) (car (cdr field)))))

;; TODO Check if call is always followed by (nset state next-space 0). If so,
;; incorporate it into the method.
(defun pgqa-deparse-newline (state indent)
  "Adjust deparse state so that deparsing continues at a new line, properly
indented."

  (let* ((indent-loc (+ indent (nref state indent)))
	 (indent-width (* indent-loc tab-width))
	 (result (nref state result)))
    (setq result (concat result "\n"))
    (setq result (concat result
			 (make-string indent-width 32)))
    (nset state result result)
    (nset state next-column indent-width)

    ;; buffer-pos might need to account for the strings added.
    (if (nref state buffer-pos)
	(nset state buffer-pos (+ (nref state buffer-pos) 1 indent-width)))

    (nset state line-empty t))
  )

(defun pgqa-deparse-space (state)
  "Write space to deparsing output."
  (let ((space (nref state next-space)))
    (nset state result
	  (concat (nref state result)
		  (make-string space 32)))
    (nset state next-column (+ (nref state next-column) space))

    ;; buffer-pos might need to account for the strings added.
    (if (nref state buffer-pos)
	(nset state buffer-pos (+ (nref state buffer-pos) space))))

  ;; Restore the default value of next-space.
  (nset state next-space 1))

;; Prepare for insertion of `str', i.e. add line break or space, as
;; needed.
(defun pgqa-deparse-string-prepare (state str indent)
  (let ((col-incr 0)
	(space (nref state next-space)))
    (if (> space 0)
	(setq col-incr (1+ col-incr)))
    (setq col-incr (+ col-incr (string-width str)))

    ;; Zero space currently can't be broken.
    ;;
    ;; TODO Consider if there are special cases not to subject to this
    ;; restriction, and maybe introduce custom variable that allows breaking
    ;; even the zero space.
    (when (and fill-column (> space 0)
	       (> (+ (nref state next-column) col-incr) fill-column))
      (pgqa-deparse-newline state indent)
      ;; No space (in addition to indentation) after newline.
      (nset state next-space 0))

    (pgqa-deparse-space state))
  )

(defun pgqa-deparse-string (state str indent)
  "Write arbitrary string to deparsing output."

  (pgqa-deparse-string-prepare state str indent)

  ;; In some cases we stick the next string to the current one w/o space
  ;; (which currently makes newline impossible - see
  ;; pgqa-deparse-string-prepare).
  (if (or
       (string= str "(") (string= str "["))
      (nset state next-space 0))

  (nset state result (concat (nref state result) str))

  (let ((str-width (string-width str)))
    (nset state next-column (+ (nref state next-column) str-width))

    ;; buffer-pos might need to account for the strings added.
    (if (nref state buffer-pos)
	(nset state buffer-pos (+ (nref state buffer-pos) str-width))))

  ;; clear line-empty if the string contains non-whitespace character.
  (if (string-match "\\S-" str)
      (nset state line-empty nil)))

;; Top-level keyword might deserve special attention, e.g. adding tabs between
;; itself and the following expression.
;;
;; `first' indicates that this is the first top-level keyword of the whole
;; query.
(defun pgqa-deparse-top-keyword (state keyword first)
  "Dump top-level keyword (SELECT, INSERT, FROM, WHERE, etc.)"

  (let ((first-offset 0)
	(nspaces))

    ;; For the structured output, all top-level keywords except for the first
    ;; one need line break.
    (when (and pgqa-multiline-query (null first))
      (pgqa-deparse-newline state 0)
      ;; No space in front of the keyword, even if the keyword does not cause
      ;; line break itself.
      (nset state next-space 0))

    ;; The first line of the deparsed query may be indented more than the rest
    ;; (see indent-estimate in pgqa-deparse).
    (if (and pgqa-multiline-query first)
	(setq first-offset
	      (- (nref state next-column)
		 (* (nref state indent) tab-width))))

    (if (and pgqa-multiline-query
	     ;; Make sure the clause is correctly indented.
	     ;;
	     ;; Right alignment of the clause keyword is also a reason to
	     ;; compute the space, even though the clause might end up on a
	     ;; new line. In such a case we'll simply put the space left from
	     ;; the keyword.
	     (or
	      (null pgqa-clause-newline)
	      pgqa-clause-keyword-right))

	(let* ((indent-top-expr (nref state indent-top-expr)))
	  (setq nspaces (- (* indent-top-expr tab-width) (string-width keyword)))

	  ;; Anything wrong about computation of indent-top-expr?
	  (if (and (null pgqa-clause-newline) (< nspaces 1))
	      (error "indent-top-expr is too low"))

	  ;; Shorten the space so that the expression (most likely column
	  ;; list) starts at the same position as the other expressions
	  ;; (e.g. table list).
	  (when (> first-offset 0)
	    (setq nspaces (- nspaces first-offset))
	    ;; Make sure at least one space is left.
	    (if (< nspaces 1)
		(setq nspaces 1)))))

    ;; If the clause keyword should be right-aligned, put the space in front
    ;; of it.
    (when pgqa-clause-keyword-right
      ;; If the clause should appear on the same line as the keyword, retain
      ;; one space to separate them. If the clause starts at a new line, use
      ;; the whole nspaces for the right alignment.
      (let ((i (if pgqa-clause-newline 0 1)))
	(while (< i nspaces)
	  (pgqa-deparse-space state)
	  (setq nspaces (1- nspaces)))))

    (pgqa-deparse-string state keyword
			 (if pgqa-multiline-query 1 0))

    ;; Separate the clause from the keyword.
    (if nspaces
	(nset state next-space nspaces))

  (if pgqa-clause-newline
      ;; Avoid the single space that pgqa-dump of pgqa-operator class puts in
      ;; front of operators.
      (nset state next-space 0)))
  )

;; In this subclass the function does not need the `indent' argument - the
;; base indentation is controlled by (nref state indent). (EIEIO does not
;; allow omitting the argument altogether.)
(defun pgqa-dump-node-query (node state indent context)
  "Turn query into a string."

  (pgqa-dump-start node state)
  ;; For mutiline output, compute the first column for expressions.
  (if pgqa-multiline-query
      (if pgqa-clause-newline
	  ;; No need to separate the keyword from the clause using a space, so
	  ;; no need to adjust indent-top-expr.
	  (nset state indent-top-expr 1)
	;; The additional space can affect indent-top-expr.
	(progn
	  ;; TODO Put the following into a separate function
	  (let ((top-clauses)
		(max-width 0)
		(indent-expr)
		(kind (nref node kind)))
	    (if (nref node target-expr)
		(push "SELECT" top-clauses))

	    (if (nref node into-expr)
		(push "INTO" top-clauses))

	    (if (nref node target-table)
		(if (string= kind "UPDATE")
		    (push "UPDATE" top-clauses)
		  (if (string= kind "INSERT")
		      (push "INSERT INTO" top-clauses))))

	    (if (nref node from-expr)
		(let ((fe (nref node from-expr)))
		  (if (> (length (nref fe from-list)) 0)
		      (push "FROM" top-clauses))
		  (if (field-boundp fe 'qual)
		      (push "WHERE" top-clauses))
		  ))

	    (if (nref node group-by)
	      (push "GROUP BY" top-clauses))
	    (if (nref node having)
	      (push "HAVING" top-clauses))
	    (if (nref node order-by)
	      (push "SORT BY" top-clauses))

	    ;; Find out the maximum length.
	    (dolist (i top-clauses)
	      (let ((width (string-width i)))
		(if (> width max-width)
		    (setq max-width width))))

	    ;; At least one space must follow.
	    (setq max-width (1+ max-width))

	    ;; Round up to the nearest multiple of tab-width.
	    (setq max-width
		  (+ max-width
		     (- tab-width (% max-width tab-width))))

	    (nset state indent-top-expr (/ max-width tab-width))))
	)
    )

  (let ((indent-clause))
    (if pgqa-clause-newline
	(setq indent-clause 1)
      ;; Extra tab might have been added in front of the clause (to ensure
      ;; that all clauses of the query start at the same position), so all
      ;; lines of the clause must start at that position.
      (setq indent-clause (nref state indent-top-expr)))

    (if (string= (nref node kind) "SELECT")
	(let ((te (nref node target-expr))
	      (ie (nref node into-expr)))
	  (pgqa-deparse-top-keyword state "SELECT" t)

	  ;; Enforce line break if necessary.
	  ;;
	  ;; TODO The same for ORDER BY, WINDOW, LIMIT, etc.
	  (if pgqa-clause-newline
	      (pgqa-deparse-newline state indent-clause))

	  (pgqa-dump te state indent-clause context)

	  (when ie
	    (pgqa-deparse-top-keyword state "INTO" nil)
	    (if pgqa-clause-newline
	      (pgqa-deparse-newline state indent-clause))
	    (pgqa-dump ie state indent-clause context))))

    (if (string= (nref node kind) "UPDATE")
	(let ((tt (nref node target-table))
	      (te (nref node target-expr)))
	  (pgqa-deparse-top-keyword state "UPDATE" t)
	  (if pgqa-clause-newline
	      (pgqa-deparse-newline state indent-clause))
	  (pgqa-dump tt state indent-clause context)

	  ;; Alias of FROM list entries are added by pgqa-dump method of
	  ;; pgqa-from-expr class because they must appear outside parentheses
	  ;; that might enclose the FROM list entry (e.g. subquery). Since we
	  ;; have a separate FROM list entry here, handle the alias
	  ;; individually.
	  (if (field-boundp tt 'alias)
	      (pgqa-dump (nref tt alias) state indent-clause context))

	  (pgqa-deparse-top-keyword state "SET" nil)
	  (if pgqa-clause-newline
	      (pgqa-deparse-newline state indent-clause))
	  (pgqa-dump te state indent-clause context)))

    (if (string= (nref node kind) "INSERT")
	(let ((tt (nref node target-table))
	      (te (nref node target-expr))
	      (ic (nref node insert-cols)))
	  (pgqa-deparse-top-keyword state "INSERT INTO" t)
	  (if pgqa-clause-newline
	      (pgqa-deparse-newline state indent-clause))
	  (pgqa-dump tt state indent-clause context)
	  (when ic
	    (nset state next-space 0)
	    (pgqa-deparse-string state "(" indent)
	    (pgqa-dump ic state indent-clause context)
	    (nset state next-space 0)
	    (pgqa-deparse-string state ")" indent))
	  (if te
	      (pgqa-deparse-top-keyword state "SELECT" nil))
	  (if pgqa-clause-newline
	      (pgqa-deparse-newline state indent-clause))
	  (pgqa-dump te state indent-clause context)
	  ))

    (if (nref node from-expr)
	(let ((from-expr (nref node from-expr)))
	  ;; Update may or may not have FROM clause.
	  (pgqa-dump from-expr state 0 context)))

    (if (nref node group-by)
      (pgqa-dump (nref node group-by) state 0 context))
    (if (nref node having)
      (pgqa-dump (nref node having) state 0 context))
    (if (nref node order-by)
      (pgqa-dump (nref node order-by) state 0 context))
    )
  (pgqa-dump-end node state))

;; Find out if from-list-entry needs to be parenthesized.
;;
;; join-rhs is t if the node is join RHS, nil if its join LHS or
;; an item of the FROM-list.
(defun pgqa-from-list-entry-needs-parens (node join-rhs)
  (if (eq (pgqa-object-class-name node) 'from-list-entry)
      (let ((is-join (= (length (nref node args)) 2))
	    (is-query (string= (nref node kind) "query")))
	;; Query needs the parentheses always, join only if it's on the right side
	;; of another join or if it has an alias.
	(or is-query (and is-join (or join-rhs (field-boundp node 'alias))))))
  )

;; Return the FROM-list subquery or nil if the entry is something else (table,
;; join, etc.).
(defun pgqa-get-from-list-subquery (fe)
  (if (and (eq (pgqa-object-class-name fe) 'from-list-entry)
	    (string= (nref fe kind) "query"))
      (car (nref fe args)))
  )

(defun pgqa-dump-node-from-expr (node state indent context)
  (pgqa-dump-start node state)

  (let ((from-list (nref node from-list))
	(indent-clause))

    ;; See the related comment in pgqa-dump method of pgqa-query class.
    (if pgqa-clause-newline
	(setq indent-clause (1+ indent))
      (setq indent-clause (nref state indent-top-expr)))

    ;; INSERT, UPDATE or DELETE statement can have the list empty.
    (if (> (length from-list) 0)
	(progn
	  (pgqa-deparse-top-keyword state "FROM" nil)

	  ;; (if pgqa-clause-newline
	  ;;     (pgqa-deparse-newline state indent-clause))

	  (let* ((i 0)
		 (fe-query)
		 (newline)
		 (parens)
		 (is-join))
	    (dolist (item from-list)
	      (if (> i 0)
		  (progn
		    (nset state next-space 0)
		    (pgqa-deparse-string state "," indent-clause)
		    ;; In general, comma should be followed by a space. As for
		    ;; special cases, we'll set it to zero explicitly below.
		    (if (null pgqa-join-newline)
			(nset state next-space 1))))

	      (setq parens (pgqa-from-list-entry-needs-parens item nil))
	      (setq is-join (= (length (nref item args)) 2))
	      (setq fe-query (pgqa-get-from-list-subquery item))

	      ;; XXX Can we do anything better than breaking the line if
	      ;; parens is t but pgqa-clause-item-newline is nil?
	      ;;
	      ;; TODO Consider newline even if pgqa-join-newline is nil but
	      ;; the next entry is a join that starts with left parenthesis
	      ;; (i.e. the join needs the parentheses itself, or its left-most
	      ;; argument does).
	      (setq newline
		    (or
		     (and (= i 0) pgqa-clause-newline)
		     (and
		      (> i 0)
		      (or (and parens pgqa-multiline-query)
			  pgqa-clause-item-newline
			  (and is-join pgqa-join-newline))
		      (null (nref state line-empty)))))

	      (if newline
		  (pgqa-deparse-newline state indent-clause))

	      ;; Set next-space to 0 to avoid unnecessary space, or even
	      ;; newline in the case of a join.
	      (if (> (nref state next-space) 0)
		  (if (or parens is-join pgqa-clause-item-newline)
		      (progn
			;; Only apply the space if non-empty line continues
			;; here.
			(if (null (nref state line-empty))
			    (pgqa-deparse-space state))
			(nset state next-space 0))
		    )
		)

	      (if (nref item lateral)
		  (pgqa-deparse-string state "LATERAL" indent))

	      (if parens
		  (pgqa-deparse-string state "(" indent))

	      (if fe-query
		  (pgqa-dump-subquery (car (nref item args))
					     state indent-clause context)
		(pgqa-dump-maybe-insertion item state indent-clause context))

              (if parens
                  (progn
                    (nset state next-space 0)
                    (pgqa-deparse-string state ")" indent-clause)))

	      (if (field-boundp item 'alias)
		  (pgqa-dump (nref item alias) state indent-clause context))
	      (setq i (1+ i)))
	    )
	  )
      )
    )

  (if (field-boundp node 'qual)
      (let ((indent-clause))
	;; Like above. XXX Should the whole body of the function be wrapped in
	;; an extra "let" construct, which initializes the variable only
	;; once?)
	(if pgqa-clause-newline
	    (setq indent-clause (1+ indent))
	  (setq indent-clause (nref state indent-top-expr)))

	;; `space' should be up-to-date as the FROM clause is mandatory.
	(pgqa-deparse-top-keyword state "WHERE" nil)
	(if pgqa-clause-newline
	    (pgqa-deparse-newline state indent-clause))
	(pgqa-dump (nref node qual) state indent-clause context)))
  (pgqa-dump-end node state))

(defun pgqa-dump-node-from-list-entry (node state indent context)
  "Print out FROM list entry (table, join, subquery, etc.)."

  (pgqa-dump-start node state)
  (let* ((args (nref node args))
	 (nargs (length args))
	 (is-join (= nargs 2))
	 (arg (car args))
	 (fe-query)
	 (parens)
	 (newline)
	 ;; If this happens to be a join nested in another one, that upper
	 ;; join might use next-space to indicate that it already took care of
	 ;; the newline.
	 (no-space (and is-join (= (nref state next-space) 0))))

    (cl-assert (or (= nargs 1) (= nargs 2)))

    (setq parens (pgqa-from-list-entry-needs-parens arg nil))
    (if (null is-join)
	(progn
	  ;; If the node is query, it'll be handled by upper node, which is
	  ;; responsible for parentheses and line breaks.
	  (setq fe-query (pgqa-get-from-list-subquery node))
	  (if (null fe-query)
	      (pgqa-dump arg state indent context)))
      (progn
	;; arg is LHS of the join.
	;;
	;; XXX Can we do anything smarter than breaking the line if parens is
	;; t but pgqa-join-newline is nil?
	(setq newline
	      (and
	       pgqa-multiline-query
	       (or parens pgqa-join-newline)
	       ;; Only break the line if it hasn't just happened for any
	       ;; reason.
	       (null (nref state line-empty))
	       ;; The purpose of no-space is explained above.
	       (null no-space)))

	(if newline
	    (progn
	      ;; TODO Shouldn't pgqa-deparse-newline set next-space to zero?
	      (pgqa-deparse-newline state indent)
	      (nset state next-space 0)))

	(if parens
	    (progn
	      ;; There should be no space in front of the next character, but
	      ;; we also set next-space to zero to indicate that nested entry
	      ;; should not be preceded by a newline.
	      (nset state next-space 0)

	      (pgqa-deparse-string state "(" indent)))

	(setq fe-query (pgqa-get-from-list-subquery arg))

	(if fe-query
	    (pgqa-dump-subquery fe-query state indent context)
	  (pgqa-dump arg state indent context))

	(if parens
	    (progn
	      (nset state next-space 0)
	      (pgqa-deparse-string state ")" indent)))

	;; Print the alias separate so that it does not appear inside the
	;; parentheses.
	(if (field-boundp arg 'alias)
	    (pgqa-dump (nref arg alias) state indent context))
	)
      )

    (when is-join
      (if pgqa-multiline-join
	  (progn
	    (nset state next-space 0)
	    (pgqa-deparse-newline state indent)))

      (let ((arg (nth 1 args)))
	(if (nref arg lateral)
	    (pgqa-deparse-string state "LATERAL" indent))

	(let ((kind (nref node kind)))
	  (if kind
	      (pgqa-deparse-string state (upcase kind) indent)))
	(pgqa-deparse-string state "JOIN" indent)

	;; The right side of a join might require parentheses.
	(setq parens (pgqa-from-list-entry-needs-parens arg t))
	;; XXX Can we do anything smarter than breaking the line if parens is
	;; t but pgqa-join-newline is nil?
	(setq newline
	      (and pgqa-multiline-query
		   (or parens pgqa-join-newline)
		   ;; Only break the line if it hasn't just happened for any
		   ;; reason.
		   (null (nref state line-empty))))

	(when newline
	  (nset state next-space 0)
	  (pgqa-deparse-newline state indent))

	(if parens
	    (pgqa-deparse-string state "(" indent))

	(setq fe-query (pgqa-get-from-list-subquery arg))

	(if fe-query
	    (pgqa-dump-subquery fe-query state indent context)
	  (pgqa-dump arg state indent context))

	(when parens
	  (nset state next-space 0)
	  (pgqa-deparse-string state ")" indent))

	;; pgqa-dump-subquery couldn't print the alias because thus it'd
	;; appear inside the parentheses.
	(if (field-boundp arg 'alias)
	    (pgqa-dump (nref arg alias) state indent context)))

      (let ((indent-on))
	(setq indent-on (if pgqa-multiline-join (1+ indent) indent))

	(when pgqa-join-on-newline
	  (pgqa-deparse-newline state indent-on)
	  (nset state next-space 0))

	(pgqa-deparse-string state "ON" indent-on)

	(pgqa-dump (nref node qual) state indent-on context)))
    )
  (pgqa-dump-end node state))

;; Dump query in the FROM list or in an expression.
;;
;; TODO Consider custom variable that controls whether parentheses are on the
;; same lines the query starts and ends respectively.
(defun pgqa-dump-subquery (query state indent context)
  (let ((state-loc state))
    (if pgqa-multiline-query
	;; Use a separate state to print out query.
	;;
	;; init-col-src of 1 stands for the opening parenthesis.
	(progn
	  (setq state-loc (pgqa-init-deparse-state
			   (+ (nref state indent) indent) 1 t
			   (nref state buffer-pos)))
	  (nset state-loc next-column (nref state next-column))
	  (nset state-loc result (nref state result))))

    (pgqa-dump query state-loc 0 context)
    (nset state result (nref state-loc result))

    (if pgqa-multiline-query
	(progn
	  (nset state next-column (nref state-loc next-column))
	  (nset state buffer-pos (nref state-loc buffer-pos)))))

  (nset state next-space 0)
)

(defun pgqa-dump-node-from-list-entry-alias (node state indent context)
  "Turn alias into a string."
  (if pgqa-print-as
      (pgqa-deparse-string state "AS" indent))
  (pgqa-dump-start node state)
  (pgqa-dump (nref node name) state indent context)
  ;; Print out argument list if there is some.
  (when (field-boundp node 'cols)
    ;; No space in front of the left parenthesis.
    (nset state next-space 0)
    (pgqa-deparse-string state "(" indent)
    (pgqa-dump (nref node cols) state indent context)
    ;; No space in front of the right parenthesis.
    (nset state next-space 0)
    (pgqa-deparse-string state ")" indent))
  (pgqa-dump-end node state))

(defun pgqa-dump-node-top-clause (node state indent context)
  (pgqa-dump-start node state)

  (let* ((indent-clause)
	 (cn (pgqa-object-class-name node))
	 (kwd)
	 (expr (nref node expr)))

    (setq kwd
	  (if (eq cn 'group-clause)
	      "GROUP BY"
	    (if (eq cn 'having-clause)
		"HAVING"
	      (if (eq cn 'sort-clause)
		  "ORDER BY"
		(error
		 (format "Unrecognized top level expression class %s." cn))))))

    (pgqa-deparse-top-keyword state kwd nil)

    ;; See the related comment in pgqa-dump method of pgqa-query class.
    (if pgqa-clause-newline
	(setq indent-clause (1+ indent))
      (setq indent-clause (nref state indent-top-expr)))

    (if pgqa-clause-newline
	(pgqa-deparse-newline state indent-clause))

    (pgqa-dump expr state indent-clause context))
  (pgqa-dump-end node state))

(defun pgqa-dump-node-func-call (node state indent context)
  "Print out function call"

  ;; Dump the function name and use pgqa-dump-start / pgqa-dump-end to mark
  ;; set the region of the operator string. This is needed so we can
  ;; eventually assign face to the string.
  (pgqa-dump-start (nth 0 (nref node gui)) state)
  (pgqa-dump (nref node name) state indent context)
  (pgqa-dump-end (nth 0 (nref node gui)) state)

  ;; No space between function name and the parenthesis.
  (nset state next-space 0)

  ;; Adjust the GUI node of the left parenthesis.
  (pgqa-dump-start (nth 1 (nref node gui)) state)
  (pgqa-deparse-string state "(" indent)
  (pgqa-dump-end (nth 1 (nref node gui)) state)

  (let ((args (nref node args)))
    (if (> (length args) 0)
	(pgqa-dump args state indent context)))
  ;; Likewise, no space after.
  (nset state next-space 0)

  ;; Adjust the GUI node of the right parenthesis.
  (pgqa-dump-start (nth 2 (nref node gui)) state)
  (pgqa-deparse-string state ")" indent)
  (pgqa-dump-end (nth 2 (nref node gui)) state)
)


(defun pgqa-dump-node-string (node state indent context)
  "Dump a string constant."

  (pgqa-dump-start node state)

  (let ((str (nref node value)))
    ;; If we're converting query to SQL string, string constants must be
    ;; quoted.
    (if (and context
	     (eq (pgqa-object-class-name context)
		 'string-to-query-context))
	(setq str (format "'%s'" str)))
    (pgqa-deparse-string state str indent))

  (pgqa-dump-end node state))

(defun pgqa-dump-node-number (node state indent context)
  "Turn number into a string."

  (pgqa-dump-start node state)

  (let ((str (nref node value)))
    (pgqa-deparse-string state str indent))

  (pgqa-dump-end node state))

(defun pgqa-dump-node-obj (node state indent context)
  "Turn an SQL object into a string."

  (pgqa-dump-start node state)
  (let ((str (mapconcat 'format (nref node args) ".")))
    (pgqa-deparse-string state str indent))
  (pgqa-dump-end node state))

(defun pgqa-dump-node-data-type (node state indent context)
  "Turn data type into a string."

  (pgqa-dump-start node state)
  (dolist (i (nref node args))
    (pgqa-dump i state indent context))

  (let ((i 0)
	(dims (nref node dims)))
    (while (< i dims)
      (nset state next-space 0)
      (pgqa-deparse-string state "[]" indent)
      (setq i (1+ i)))
    )
  (pgqa-dump-end node state))

(defun pgqa-dump-node-case (node state indent context)
  "Print out the CASE expression."

  (pgqa-dump-start node state)

  ;; The multi-line output should start exactly at the indentation which is
  ;; determined by the indent argument, so new line is needed.
  (when (and pgqa-multiline-case (null (nref state line-empty)))
    (pgqa-deparse-newline state indent)
    (nset state next-space 0))

  (pgqa-deparse-string state "CASE" indent)

  (let ((indent-2 indent))
    (if pgqa-multiline-case
      (setq indent-2 (1+ indent-2)))

    (if (nref node arg)
	;; Even the argument deserves extra indentation, in case it spans
	;; multiple lines.
	(pgqa-dump (nref node arg) state indent-2 context))

    (dolist (branch (nref node branches))
      (when pgqa-multiline-case
	(pgqa-deparse-newline state indent-2)
	(nset state next-space 0))
      (pgqa-dump branch state indent-2 context))

    (when (field-boundp node 'else)
      (when pgqa-multiline-case
	(pgqa-deparse-newline state indent-2)
	(nset state next-space 0))
      (pgqa-deparse-string state "ELSE" indent-2)
      (pgqa-dump (nref node else) state indent-2 context))
    )
  (when pgqa-multiline-case
    (pgqa-deparse-newline state indent)
    (nset state next-space 0))
  (pgqa-deparse-string state "END" indent)
  (pgqa-dump-end node state))

(defun pgqa-dump-node-case-branch (node state indent context)
  "Print out a single branch (WHEN ... THEN ...) of the CASE expression."

  (pgqa-dump-start node state)
  (let* ((args (nref node args))
	 (when-expr (car args))
	 (then-expr (car (cdr args)))
	 (indent-2 indent))
    (pgqa-deparse-string state "WHEN" indent)

    ;; The arguments should get extra indentation, if they span multiple
    ;; lines. (Not sure it looks nice if pgqa-multiline-case is sufficient to
    ;; cause this extra indentation.)
    (if pgqa-multiline-case-branch
	(setq indent-2 (1+ indent-2)))

    (pgqa-dump when-expr state indent-2 context)

    (when pgqa-multiline-case-branch
      (pgqa-deparse-newline state indent)
      (nset state next-space 0))

    (pgqa-deparse-string state "THEN" indent)
    (pgqa-dump then-expr state indent-2 context))

  ;; If the WHEN and THEN clauses are separated by a newline, the whole CASE
  ;; expression looks better if an extra line break is added in front of the
  ;; next branch (or in front of the ELSE clause).
  (if pgqa-multiline-case-branch
      (pgqa-deparse-newline state 0))
  (pgqa-dump-end node state))

(defun pgqa-child-op-needs-parens (node arg)
  "Find out if argument of an operator node should be parenthesized."
  (if
      (or
       ;; Query as an argument should always be parenthesized.
       (eq (pgqa-object-class-name arg) 'query)

       ;; Sublink can have either query or an (array) expression as an
       ;; argument, both of which should always be parenthesized.
       (eq (pgqa-object-class-name node) 'sublink))
      t
    (let ((prec (nref node prec))
	  (prec-child
	   (if (pgqa-object-of-class-p arg 'operator)
	       (nref arg prec))))
      (and prec prec-child (> prec prec-child))))
  )

(defun pgqa-is-multiline-operator (node)
  "Should the argument be printed in structured way?"

  (if (and pgqa-multiline-operator
	   (or
	    (pgqa-object-of-class-p node 'operator)
	    (and
	     pgqa-multiline-case
	     (pgqa-object-of-class-p node 'case))))

      ;; List is not a multi-line operator as such, although its arguments
      ;; can be.
      (null (pgqa-object-of-class-p node 'node-list))
    )
  )

;; indent relates to the operator, not argument.
(defun pgqa-indent-operator-first-argument (state indent arg-idx)
  "Prepare position for the first argument of a multiline operator."

  (let* ((s (nref state result))
	 (i (1- (length s))))
    (if
	;; No duplicate newline if we already have one.
	(and pgqa-clause-newline (= arg-idx 0) (nref state line-empty))
	(let ((indent-extra
	       (-
		;; indent argument of the function is relative to (nref state
		;; indent), so compute "absolute value".
		(+ (nref state indent) indent)
		(/ (nref state next-column) tab-width)))
	      (indent-extra-spaces))
	  ;; indent is for the operator, so add 1 more level for the argument.
	  (setq indent-extra (1+ indent-extra))
	  (setq indent-extra-spaces (* indent-extra tab-width))
	  (nset state result
		(concat (nref state result)
			(make-string indent-extra-spaces 32)))
	  (if (nref state buffer-pos)
	      (nset state buffer-pos (+ (nref state buffer-pos)
					indent-extra-spaces))))
      (progn
	(pgqa-deparse-newline state (1+ indent))
	(nset state next-space 0))))
  )

(defun pgqa-dump-node-operator (node state indent context)
  "Turn operator expression into a string."

  (pgqa-dump-start node state)
  (let* ((args (nref node args))
	 (nargs (length args))
	 (op (nref node op))
	 (is-list (pgqa-object-of-class-p node 'node-list))
	 (is-unary
	  (null
	   (or (cdr args) is-list)))
	 (is-cast (string= op "::"))
	 (i 0)
	 (multiline
	  (and
	   pgqa-multiline-operator
	   (pgqa-is-multiline-operator node)))
	 (arg-multiline-prev))

    (dolist (arg args)
      (let* ((parens (pgqa-child-op-needs-parens node arg))
	     (state-backup)
	     (arg-is-operator (pgqa-object-of-class-p arg 'operator))
	     (arg-is-list (pgqa-object-of-class-p arg 'node-list))
	     (arg-is-te (eq (pgqa-object-class-name arg) 'target-entry))
	     ;; FROM list entry?
	     (arg-is-fe (eq (pgqa-object-class-name arg) 'from-list-entry))
	     (arg-is-query (eq (pgqa-object-class-name arg) 'query))
	     (arg-multiline)
	     (indent-arg indent)
	     (gui (nref node gui)))

	(if arg-is-te
	    (setq arg-multiline (pgqa-is-multiline-operator (nref arg expr)))
	  (setq arg-multiline (pgqa-is-multiline-operator arg)))

	(if (or (and is-unary (null (nref node postfix))) (> i 0))
	    (let ((omit-space))

	      ;; Never put space in front of comma or cast operator.
	      (setq omit-space (or is-list is-cast))

	      ;; Should each arg start at a new line?
	      (when multiline
		(pgqa-deparse-newline state indent)
		(setq omit-space t))

	      (if omit-space
	      	  (nset state next-space 0))

	      ;; BETWEEN-AND operator has 2 keywords, and "gui" is a 2-element
	      ;; list containing one node per keyword.
	      (if (equal op "BETWEEN")
		  (if (= i 1)
		      (setq gui (car gui))
		    (when (= i 2)
		      (setq op "AND")
		      (if gui
			  (setq gui (car (cdr gui))))
		      )
		    )
		)

	      ;; Use pgqa-dump-start / pgqa-dump-end to mark set the region of
	      ;; the operator string. This is needed so we can eventually
	      ;; assign face to the string.
	      (if gui
		  (pgqa-dump-start gui state))

	      (pgqa-deparse-string state op indent)

	      ;; Cast operator (::) is special in that it should not be
	      ;; separated from the target type by space.
	      (if is-cast
		  (nset state next-space 0))

	      (if gui
		  (pgqa-dump-end gui state)))

	  )

	;; Ensure correct initial position for the argument output in case the
	;; operator spans multiple lines.
	(if multiline
	    (if (or
		 ;; "(" should appear on a new line, indented as the argument
		 ;; would be if there were no parentheses. (The argument itself
		 ;; will eventually be given extra indentation.)
		 parens

		 (and
		  ;; If the arg is also an operator, it'll take care of the
		  ;; initial line preparation itself.
		  (null arg-is-operator)

		  ;; If the current operator (i.e. not the argument) is unary
		  ;; prefix (not enclosed in parentheses) - it looks better if
		  ;; both operator and argument end up on the same
		  ;; line. Again, if the argument of the unary operator is
		  ;; operator itself (which includes parentheses), it'll take
		  ;; care of the preparation itself.
		  (or (> (length args) 1) (nref node postfix))
		  )
		 )
		(pgqa-indent-operator-first-argument state indent i)
	      )

	  (when
	      (or
	       (and is-list

		    ;; Multi-line argument takes care of the initial line
		    ;; preparation itself.
		    (null arg-multiline)

		    (or
		     ;; If an "ordinary" expression follows a multi-line
		     ;; operator within comma operator (e.g. SELECT list),
		     ;; break the line so that the multi-line operator does not
		     ;; share even a single line with the current argument.
		     ;;
		     ;; TODO Consider a custom variable to switch this behavior
		     ;; on / off.
		     arg-multiline-prev

		     ;; Definitely break the line if user requires each target
		     ;; list / from list entry to begin on a new line.
		     ;;
		     ;; Do nothing for i = 0 because pgqa-clause-item-newline
		     ;; requires pgqa-clause-newline to be set, which takes
		     ;; care of the first entry. Only take action if the comma
		     ;; is a target list of FROM list (i.e. do not affect
		     ;; things like function argument list).
		     (and pgqa-clause-item-newline (> i 0)
			  (or arg-is-te arg-is-fe))))

	       ;; Another reason to break the line is a query in an expression
	       ;; (e.g. IN). If we didn't break the line here, the first line
	       ;; (typically SELECT) would be much more indented than the
	       ;; following lines.
	       (and pgqa-multiline-query arg-is-query))

	    (pgqa-deparse-newline state indent)
	    (nset state next-space 0))
	  )

	(when parens
	  ;; In case we're not at the start of a new line, so make sure that
	  ;; there's no space between the sublink keyword and the left
	  ;; parenthesis. XXX Consider customization variable that allows such
	  ;; a space.
	  (if (eq (pgqa-object-class-name node) 'sublink)
	      (nset state next-space 0))
	  (pgqa-deparse-string state "(" indent)
	  ;; No space, whatever follows "(".
	  (nset state next-space 0))

	;; Comma needs special treatment because it doesn't look nice if it's
	;; the first character on a line.
	;;
	;; Backup the slots but keep the instance so that callers still see
	;; our changes.
	(setq state-backup (pgqa-deparse-state-get-attrs state))

	;; Serialize the argument now, giving it additional indentation if
	;; user wants the output structured.
	(let ((indent-extra 0))
	  (when multiline
	    (setq indent-extra (1+ indent-extra))
	    ;; The extra indentation due to parentheses, mentioned above. It
	    ;; only applies to operators, not to subqueries.
	    (if (and parens (null arg-is-query))
		(setq indent-extra (1+ indent-extra))))
	  (setq indent-arg (+ indent indent-extra))

	  ;; Operator can be contained in an SQL string insertion.
	  (pgqa-dump-maybe-insertion arg state indent-arg context))

	(when
	    ;; If the argument has reached fill-column, line should have
	    ;; broken in front of the argument. So restore the previous state
	    ;; and dump the argument again, with fill-column temporarily
	    ;; decreased by one. That should make the argument appear on the
	    ;; new line too.
	    (and
	     (>= (nref state next-column) fill-column)
	     (< i (1- nargs)))

	  (pgqa-deparse-state-set-attrs state state-backup)

	  ;; TODO Loop until the line is broken correctly, but don't let
	  ;; fill-column reach value that lets little or no space on the
	  ;; line. But only try once if the related custom variable(s) allow
	  ;; for line break between opening paren and the following character
	  ;; or closing paren and the preceding character.
	  (let ((fill-column (1- fill-column)))
	    (pgqa-dump-maybe-insertion arg state indent-arg context))
	  )

	(when parens
	  ;; The closing parenthesis should be on a separate line, like the
	  ;; opening one.
	  (when
	      (and multiline
		   ;; Row expression is not considered a multi-line operator,
		   ;; so it looks better if the ")" is stuck to it.
		   ;;
		   ;; TODO Verify the behavior when the last expression of the
		   ;; row is a multi-line operator.
		   (or (null arg-is-list) (= (length (nref arg args)) 1))

		   ;; Subquery in an operator should have both opening and
		   ;; closing parenthesis attached tightly to it, just like it
		   ;; works for subquery in the FROM list.
		   (null arg-is-query))
	    (pgqa-deparse-newline state (1+ indent)))
	  ;; Never space in front of ")".
	  (nset state next-space 0)
	  (pgqa-deparse-string state ")" indent))

	(when (and is-unary (nref node postfix))
	  ;; TODO This part appears above for binary and unary prefix
	  ;; operators. Move it into a new function (pgqa-deparse-op-string?)
	  (if (nref node gui)
	      (pgqa-dump-start-gui (nref node gui) state))

	  (pgqa-deparse-string state op indent)

	  (if (nref node gui)
	      (pgqa-dump-end-gui (nref node gui) state)))

	(setq i (1+ i))

	(setq arg-multiline-prev arg-multiline))
      )
    )
  (pgqa-dump-end node state))

(defun pgqa-dump-node-target-entry (node state indent context)
  "Turn target list entry into a string."

  (pgqa-dump-start node state)
  (pgqa-dump (nref node expr) state indent context)

  (when (field-boundp node 'alias)
    (if pgqa-print-as
	(pgqa-deparse-string state "AS" indent))
    (pgqa-dump (nref node alias) state indent context))

  (pgqa-dump-end node state))

(defun pgqa-dump-node-alias (node state indent context)
  "Turn target list entry alias into a string."

  (pgqa-dump-start node state)
  (pgqa-deparse-string state (nref node name) indent)
  (pgqa-dump-end node state))

(defun pgqa-dump-node-alias-arg (node state indent context)
  "Turn an argument of a target list entry alias into a string."

  (pgqa-dump-start node state)
  (pgqa-deparse-string state (nref node var) indent)
  (if (field-boundp node 'datatype)
      (pgqa-dump (nref node datatype) state indent context))
  (pgqa-dump-end node state))

(provide 'pgqa-dump)
