;; Copyright (C) 2016 Antonin Houska
;;
;; This file is part of PGQA.
;;
;; PGQA is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; PGQA is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License qalong
;; with PGQA. If not, see <http://www.gnu.org/licenses/>.


(require 'seq)
(require 'semantic)
;(require 'wisent)
(require 'pgqa-dump)
(require 'pgqa-node)
(require 'pgqa-parser)
;; pgqa-dump-raw contains macros.
(eval-when-compile
  (require 'pgqa-dump-raw)
  (require 'pgqa-node-api))

(defgroup pgqa nil
  "PGQA mode user settings."
  :group 'programming-group)

(defvar pgqa-log-buffer "*pgqa-log*"
  "Name of buffer to which PGQA writes problems it found in SQL queries.")

;; A list of `pgqa-query-string-insertion' instances.
(defvar-local pgqa-sql-string-insertions nil)

(defvar-local pgqa-last-region nil
  "A list of two markers, pointing at the start and the end of the source text
of the last processed query respectively.")

;; Markers let user interact with the query, so we refer to them as GUI.
(defvar-local pgqa-query-markers nil
  "List of markers located within the current query.")

(defun pgqa-setup-query-gui (query trim)
  "Add markers and text properties to query nodes.

`trim' tells that leading whitespace should not be included in node regions."

  (pgqa-walk query 'pgqa-setup-node-gui trim))

(defun pgqa-delete-query-gui ()
  "Make markers available for garbage collection."

  (when pgqa-query-markers
    (dolist (m pgqa-query-markers)
      (set-marker m nil))
    (setq pgqa-query-markers nil))
  )

(defun pgqa-set-query-faces (query)
  "Add faces to query nodes."
  (pgqa-walk query 'pgqa-set-node-face nil))

(defun pgqa-reset-query-faces (query)
  "Remove previously added faces from query nodes."
  (pgqa-walk query 'pgqa-reset-node-face nil))

;; XXX Consider defcustom instead.
(defvar pgqa-mode-prefix-key "\C-c")

(defun pgqa-customize ()
  (interactive)
  (customize-group "pgqa" t))

(defvar pgqa-mode-prefix-map
  (let ((map (make-sparse-keymap)))
    (define-key map "+" '(menu-item "Customize" pgqa-customize
				    :visible t))
    (define-key map ")" '(menu-item "Send Region" pgqa-send-region
				    :visible (use-region-p)))
    (define-key map "{" '(menu-item "Query To String" pgqa-query-to-string
				    :visible t))
    (define-key map "}" '(menu-item "String To Query" pgqa-string-to-query
				    :visible t))
    (define-key map "|" '(menu-item "Create String Insertion" pgqa-create-insertion
				    :visible (use-region-p)))
    (define-key map "<" '(menu-item "Format Query" pgqa-format-query
				    :visible t))
    (define-key map ">" '(menu-item "Parse Query" pgqa-parse
				    :visible t))
    map))

(defvar pgqa-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map [menu-bar pgqa] (cons "PGQA" pgqa-mode-prefix-map))
    (define-key map pgqa-mode-prefix-key pgqa-mode-prefix-map)
    map))

(defvar pgqa-mode-syntax-table
  (let ((st (make-syntax-table))
	(op-chars))
    ;; Characters that constitute SQL operators are best handled as
    ;; punctuation.
    ;;
    ;; TODO Modify the entries for characters that do not belong to
    ;; pgqa-char-terminals-non-grouped, e.g. colon - it should be punctuation
    ;; because it's contained in double-colon (i.e. cast operator), but is not
    ;; legal alone. Find out if other operators like this exist. However, if
    ;; it's hard for lexer to accept such "multi-character punctuations",
    ;; introduce special kind of token for them.

    ;; First, collect all the characters to be possibly contained in
    ;; operators.
    (dolist (g pgqa-operator-groups)
      (let ((ops (cdr (cdr g))))
	(dolist (op ops)
	  (if (= (string-width op) 1)
	      (push (string-to-char op) op-chars)))))
    (setq op-chars (append op-chars pgqa-char-terminals-non-grouped))

    ;; Now adjust the syntax table accordingly.
    (dolist (i op-chars)
      (let ((cs (char-syntax i)))
	;; Do not change syntax class of terminals indispensable as opening
	;; and closing parentheses - these are not likely to be used in
	;; operators in valid PG expression.
	(if (null (or (eq cs ?\() (eq cs ?\))))
	    (modify-syntax-entry i "." st))))

    ;; Underscore is a legal component of SQL identifiers.
    (modify-syntax-entry ?_ "w" st)

    ;; Single quote is punctuation character in the parent table, but we need
    ;; it to denote strings.
    (modify-syntax-entry ?' "\"" st)
    ;; In addition to its "punctuation" role, dash is a comment starter.
    (modify-syntax-entry ?- ". 12" st)
    (modify-syntax-entry ?\n "> " st)

    ;; Asterix should constitute for a symbol rather than punctuation. That
    ;; makes more sense if we want to use it as a column wildcard. However
    ;; this distinction only matters to syntax highlighting. As for the
    ;; grammar we need a special class for it anyway, see
    ;; get-next-query-token.
    (modify-syntax-entry ?* "w" st)

    ;; All the other, ordinary punctuations.
    (modify-syntax-entry ?= "." st)

    st)
  "Syntax table used while in 'pgqa-mode'.")

;; Keywords that parser does not understand (so far?), but user expect them to
;; be highlighted.
;;
;; TODO Consider if strings are safer than symbols, in terms of conflict with
;; other elisp modules.
(defvar pgqa-keywords-highlight-only
  '(
   BEGIN COMMENT CREATE DECLARE EXCEPTION FUNCTION IF
   LANGUAGE RETURN RETURNS TABLE))

;; Turn pgqa-keywords-base list into a regular expression.
(defun pgqa-keywords ()
  (list (concat "\\<"
		(mapconcat 'symbol-name
			   (append pgqa-keyword-symbols
				   pgqa-keywords-highlight-only
				   (mapcar 'make-symbol
					   pgqa-keyword-operators)
				   )
			   "\\>\\|\\<")
		"\\>")))

;; We add the -face suffix although the Elisp reference does not recommend
;; so. Without the prefix we'd end up with name conflicts between faces and
;; EIEIO classes.
(defface pgqa-operator-face
  '((t :foreground "red1"))
  "`pgqq-mode' face used to highlight SQL operators."
  :group 'pgqa)

(defface pgqa-func-call-face
  '((t :foreground "salmon"))
  "`pgqq-mode' face used to highlight SQL functions."
  :group 'pgqa)

(defface pgqa-sql-string-insertion-face
  '((t :foreground "yellow"))
  "`pgqq-mode' face used to highlight SQL string insertions."
  :group 'pgqa)

;;;###autoload
(add-to-list 'auto-mode-alist (cons "\\.sql\\'" 'pgqa-mode))

;; TODO Make sure the funcion can also turn the mode off.

;;;###autoload
(define-derived-mode pgqa-mode
  prog-mode
  "PGQA"
  "Major mode to parse and analyze SQL queries, with respect to the SQL \
dialect and concepts of query processing that PostgreSQL uses."

  ;; GNU Emacs 25 was available when the PGQA development started. Don't let
  ;; us accept the burden of supporting older versions.
  ;;
  ;; Raising the error during mode initialization seems relatively good
  ;; place. It does not prevent user from calling functions manually (and
  ;; possibly getting various errors), doing the check in the individual
  ;; functions is not appropriate either.
  (if (< emacs-major-version 25)
      (error "PGQA requires GNU Emacs 25 or newer"))

  (setq-local font-lock-defaults
	      '((pgqa-keywords) nil t nil))

  ;; As we don't have a regular comment starter, the lexers' regular
  ;; expression needs to be adjusted.
  (setq-local semantic-lex-comment-regex "--.*")

  ;; For fill-column function to work properly.
  (setq-local comment-start "--"))

;; This function should be called by all features that involve multiple
;; consequent operations on the same part of the buffer. Therefore we set
;; pgqa-last-region even if the whole buffer should be processed.
;;
;; XXX Consider narrowing.
(defun pgqa-set-region ()
  "Update `pgqa-last-region' according to the current region."

  (let ((m-start)
	(m-end))
    ;; When looking for the source text, marked region has the highest
    ;; precedence. If nothing is marked, the most recent region is used. If no
    ;; region was marked so far, use the whole buffer.
    ;;
    ;; Since region-beginning / region-end can return either integer or mark,
    ;; and since we want to avoid creating an extra mark in the latter case,
    ;; the easiest way to handle the change is to create a new list of markers
    ;; from scratch and delete the original one.
    (if mark-active
	(progn
	  (let ((m))
	    (setq m (region-beginning))
	    (if (markerp m)
		(setq m-start m)
	      (setq m-start (make-marker))
	      (set-marker m-start m))
	    ;; Make the existing marker available for garbage collection.
	    (when pgqa-last-region
	      (setq m (car pgqa-last-region))
	      (set-marker m nil))

	    (setq m (region-end))
	    (if (markerp m)
		(setq m-end m)
	      (setq m-end (make-marker))
	      (set-marker m-end m))

	    (when pgqa-last-region
	      (setq m (car (cdr pgqa-last-region)))
	      (set-marker m nil)))

	    (deactivate-mark))

      ;; Mark is not active.
      (if pgqa-last-region
	  ;; The previous selection should stay intact.
	  (progn
	    (setq m-start (car pgqa-last-region))
	    (setq m-end (car (cdr pgqa-last-region))))
	;; pgqa-last-region needs to be initialized.
	(setq m-start (make-marker))
	(set-marker m-start (point-min))
	(setq m-end (make-marker))
	(set-marker m-end (point-max))))

    ;; The insertion type is such that inserted text stays within the markers
    ;; even if they are at the beginning and the end of the buffer
    ;; respectively.
    (set-marker-insertion-type m-start nil)
    (set-marker-insertion-type m-end t)

    ;; Update pgqa-last-region.
    (setq pgqa-last-region (list m-start m-end))))

;; Suppress compiler warning.
(declare-function pgqa-check-customizations "pgqa-dump" ())

;;;###autoload
(defun pgqa-format-query (&optional indent)
 "Format SQL query that the current buffer contains. If region is active,
only the selected part of the buffer is processed.

The optional prefix argument INDENT tells how much should the query be
indented. If it's passed, then INDENT times `tab-width' spaces are inserted
in front of each line. Without it, the command tries to guess the indentation
from first position of the query."
 (interactive "P")

 ;; The customizations do not affect parsing, but by checking early we avoid
 ;; wasting effort on parsing.
 (pgqa-check-customizations)

 ;; Don't set markers during parsing.
 (pgqa-parse t)

 (if (> (length pgqa-sql-string-insertions) 0)
     ;; Make nodes aware of insertions. The insertion positions will need to
     ;; be adjusted during the actual formatting.
     (pgqa-assign-insertions-to-nodes
      pgqa-query-tree
      (vector pgqa-sql-string-insertions)))

 (pgqa-deparse indent nil)

 (when (> (length pgqa-sql-string-insertions) 0)
   ;; Adjust start and end position of each insertion so it matches the
   ;; formatted query. Since insertions should not be used neither outside
   ;; pgqa-mode nor in the interactive mode, we assume that
   ;; pgqa-setup-query-gui has been run, so the start positions of nodes have
   ;; been fixed.
   (pgqa-adjust-insertions pgqa-query-tree)

   ;; Set the insertion face.
   (pgqa-set-sql-string-insertion-face pgqa-sql-string-insertions))
 )

(defun pgqa-parse-query-batch ()
  "Read SQL query from the standard input, parse it and write it to the
standard output."

  (if (null noninteractive)
      (user-error "pgqa-parse-query-batch function should only be used in
batch mode"))

  (pgqa-mode)

  ;; The region info is currently not interesting for test. (In fact it would
  ;; make them more fragile.)
  (setq pgqa-query-tree-print-region nil)

  (let ((state))
    (pgqa-parse)
    (setq state
	  (make-instance pgqa-dump-raw-state
			 :node-start 'pgqa-node-to-lisp-start
			 :node-end 'pgqa-node-to-lisp-end
			 :result ""))
    (pgqa-dump-raw pgqa-query-tree state 0)
    (princ (nref state result))))

(defun pgqa-format-query-batch ()
  "Read SQL query from the standard input, format it and write it to the
standard output."

  (if (null noninteractive)
      (user-error "pgqa-format-query-batch function should only be used in
batch mode"))

  (pgqa-mode)

  (let ((state))
    (pgqa-check-customizations)
    (pgqa-parse t)
    (setq state (pgqa-deparse-batch))
    (princ (nref state result))))

;; define-lex will eventually define the function, however byte compiler
;; complains if it's not defined earlier.
(defun pgqa-sql-string-lexer (start end &optional depth length))

(defvar-local pgqa-sql-string-lexer-inited nil)

(defun pgqa-set-sql-string-insertion-face (insertions)
  "Add face to the existing SQL string insertions."

  (dolist (insertion insertions)
    (let ((m-start (nref insertion start))
	  (m-end (nref insertion end))
	  (o))
      ;; Create an overlay that ensures highlighting of the insertion and make
      ;; the insertion object aware of it for cleanup purposes.
      ;;
      ;; FRONT-ADVANCE and REAR-ADVANCE arguments correspond to the marker
      ;; insertion types applied in `pgqa-set-insertion-markers'.
      (setq o (make-overlay m-start m-end (current-buffer) nil t))

      ;; Overlay is indispensable here, as opposed to text properties. The
      ;; point is that user might want to edit the insertion text before
      ;; parsing / formatting. That includes adding text at the beginning /
      ;; end of the insertion. Overlay seems to be the only means to ensure
      ;; that the added text also has the insertion face.
      (overlay-put o 'font-lock-face 'pgqa-sql-string-insertion-face)
      (nset insertion overlay o))
    )
  )

(defun pgqa-reset-sql-string-insertion-face ()
  "Remove face from the existing SQL string insertions."

  (dolist (insertion pgqa-sql-string-insertions)
    (let ((m-start (nref insertion start))
	  (m-end (nref insertion end))
	  (o))
      (setq o (nref insertion overlay))
      (delete-overlay o))
    )
  )

(defun pgqa-delete-sql-string-insertions ()
  "Remove the existing SQL string insertions."

  ;; Remove the face until we forget where they were set.
  (pgqa-reset-sql-string-insertion-face)

  ;; Make the markers available for garbage collection.
  (dolist (insertion pgqa-sql-string-insertions)
    (let ((m-start (nref insertion start))
	  (m-end (nref insertion end)))
      (set-marker m-start nil)
      (set-marker m-end nil)))

  ;; Do the same for the list itself.
  (setq pgqa-sql-string-insertions nil))

;; `insertions' is a single-element array that contains the actual list of
;; insertions, ordered by buffer position. Once the first insertion gets
;; processed (i.e. the first node not contained in it is reached), the list is
;; replaced with a new one that starts with the 2nd element, etc.
(defun pgqa-assign-insertions-to-nodes (query insertions)
  "Assign an insertion key to each node that is contained in the insertion"
  (pgqa-walk query 'pgqa-assign-insertion-to-node insertions))

(defun pgqa-assign-insertion-to-node (node context)
  "Assign an insertion key to a single node if matching insertion exists."
  (let* ((container context)
	 (search t)
	 (insertions)
	 (insertion)
	 (node-start)
	 (node-end)
	 (region)
	 (reg-start)
	 (reg-end)
	 (ins-start)
	 (ins-end))

    ;; The node should not have the insertion assigned so far.
    (cl-assert (null (nref node insertion)))

    (setq region (nref node region))
    (setq node-start (elt region 0))
    (setq node-end (elt region 1))

    ;; While searching for the next possibly useful insertion we rely on the
    ;; fact that consecutive calls of this function should generate
    ;; non-decreasing sequence of node-start values. Thus if we move to the
    ;; next insertion we don't have to worry that the previous one can still
    ;; match any node to come.
    (while search
      (setq insertions (aref container 0))
      (if insertions
	  (progn
	    (setq insertion (car insertions))
	    (setq ins-end (nref insertion end))
	    (if (< ins-end node-start)
		;; This insertion cannot affect this node as well as any
		;; following. Move to the next one and make the new list
		;; available for the next call(s).
		(progn
		  (setq insertions (cdr insertions))
		  (aset container 0 insertions))
	      ;; This insertion does overlap with the node, so exit the loop
	      ;; and check.
	      (setq search nil)))
	;; All insertions are processed, exit the loop.
	(setq search nil)))

    (when insertions
      (setq ins-start (nref insertion start))
      ;; Assign the insertion to the node if the whole node is contained in
      ;; it.
      (if (and (>= node-start ins-start) (<= node-end ins-end))
	  (nset node insertion insertion)))))

(defun pgqa-adjust-insertions (query)
  "Update start and end positions of insertions according to
  just-formatted query."

  (let ((context (vector nil)))
    (pgqa-walk query 'pgqa-adjust-insertion context)))

;; context contains the insertion key of the previous node. It's pased a as
;; single-element vector so it can be used as both input and output argument
;;
;; Like in pgqa-assign-insertion-to-node we rely on node start position to be
;; non-decreasing sequence throughout the recursion.
(defun pgqa-adjust-insertion (node context)
  "Update start and end posiiton of a single insertion according
  to just-formatted query."

  (let ((cur (nref node insertion))
	(cur-start)
	(cur-end)
	(prev (elt context 0))
	(node-markers)
	(node-start)
	(node-end))
    (when cur
      (setq node-markers (nref node markers))
      (setq node-start (elt node-markers 0))
      (setq node-end (elt node-markers 1))
      (setq cur-start (nref cur start))
      (setq cur-end (nref cur end))
      (if (and (null prev) (eq cur prev))
	  ;; The same insertion as the previous node had. Only update the end
	  ;; marker.
	  (set-marker cur-end (marker-position node-end))
	(progn
	  ;; A different insertion from that of the previous node. Set both
	  ;; start and end position of the insertion as we don't know if the
	  ;; next node will reference this insertion.
	  (set-marker cur-start (marker-position node-start))
	  (set-marker cur-end (marker-position node-end)))
	)
      )
    ;; Store the current insertion. This will become prev for the next node.
    (aset context 0 cur))
  )

(defun pgqa-string-to-query ()
  "Functions written in the PL/pgSQL procedural language sometimes construct SQL
query by concatenating string constants and string variables. This function
extracts such a query from the containing string so it can be processed
further."

  (interactive)

  ;; The feature would be difficult to use without markers and overlays. See
  ;; text-only variable in query-deparse.
  (if (or (null (equal major-mode 'pgqa-mode)) noninteractive)
      (error "pgqa-string-to-query cannot be used outside pgqa-mode or in batch mode."))

  ;; Cleane up the existing insertions, e.g. after having getting back to the
  ;; string via Undo.
  (if (> (length pgqa-sql-string-insertions) 0)
      (pgqa-delete-sql-string-insertions))

  ;; Initialize the lexer if not done yet.
  (unless pgqa-sql-string-lexer-inited
    (define-lex-regex-analyzer
      pgqa-sql-concat-lexer
      "Detect any unrecognized character."
      "||"
      (error "Unrecognized token"))

    (define-lex-simple-regex-analyzer
      pgqa-sql-concat-lexer
      "Recognize SQL concatenation operator."
      "||"
      'concat)

    (define-lex-regex-analyzer
      pgqa-sql-string-lexer-error
      "Detect any unrecognized character."
      "."
      (error "Unrecognized token"))

    (define-lex
      pgqa-sql-string-lexer
      "Lexer responsible for tokenization of a query in the form of
SQL string."

      ;; Expect the query as mixture of strings and symbols,
      ;; separaated by the SQL concatenation operator (||).
      semantic-lex-ignore-comments
      semantic-lex-ignore-whitespace
      semantic-lex-ignore-newline
      semantic-lex-string-sql
      pgqa-sql-concat-lexer
      semantic-lex-symbol-or-keyword
      pgqa-sql-string-lexer-error)

    (semantic-lex-init)
    (setq pgqa-sql-string-lexer-inited t))


  ;; Initialize or update pgqa-last-region.
  (pgqa-set-region)

  (let ((start)
	(end)
	(tokens)
	(token-kind)
	(ntokens)
	(i 0)
	(is-concat)
	(prev-concat)
	(positions)
	(pos-start)
	(pos-end)
	(pos-query)
	(str)
	(str-width)
	(nremoved 0)
	(query ""))

    ;; Get the start and end positions wherever it's available.q
    (let ((m-start (car pgqa-last-region))
	  (m-end (car (cdr pgqa-last-region))))
      (setq start (marker-position m-start))
      (setq end (marker-position m-end)))

    (setq tokens (pgqa-sql-string-lexer start end))
    (setq ntokens (length tokens))

    ;; Keep track of the position within the query being created. This is
    ;; needed to setup insertion markers.
    (setq pos-query start)

    (dolist (token tokens)
      (setq token-kind (car token))
      (setq positions (cdr token))

      ;; The positions need to reflect replacement of double quotations with
      ;; single ones.
      (setq pos-start (- (car positions) nremoved))
      (setq pos-end (- (cdr positions) nremoved))

      ;; Replace double apostrophes / quotation marks with single ones. Do not
      ;; touch tokens consisting of 2 apostrophes --- these represent empty
      ;; string and would get broken by this step.
      (when (> (- pos-end pos-start) 2)
	(goto-char pos-start)
	(while (search-forward "''" pos-end t)
	  (replace-match "'" nil t)
	  (setq nremoved (1+ nremoved))
	  (setq pos-end (1- pos-end)))
	)

      (setq str (buffer-substring-no-properties pos-start pos-end))

      ;; The concatenation operator will not appear in the query, so it does
      ;; not affect pos-query.
      (if (eq (car token) 'concat)
	  (setq str-width 0)
	(setq str-width (- pos-end pos-start)))

      ;; We cannot check correctness of the query itself right now, but some
      ;; obviously wrong cases can be excluded immediately.
      (if (and (eq i 0) (eq token-kind 'concat))
	  (user-error "The query string must not start with ||")
	(if (and (eq i (1- ntokens)) (eq token-kind 'concat))
	    (user-error "The query string must not end with ||")))

      ;; Check if the string components are interleaved with concatenation
      ;; operators.
      (setq is-concat (eq token-kind 'concat))
      (when (and is-concat prev-concat)
	(goto-char pos-start)
	(user-error "Two consecutive || operators found"))
      (when (and (> i 0) (null is-concat) (null prev-concat))
	(goto-char pos-start)
	(user-error "Missing || operator"))
      (setq prev-concat is-concat)

      ;; Concatenate the parts, omitting the concatenation operators
      ;; themselves.
      (unless (eq (car token) 'concat)
	;; We're not interested in the apostrophes.
	(when (eq token-kind 'string)
	  ;; The apostrophes should always be there.
	  (cl-assert (>= (length str) 2))
	  (setq str (substring str 1 -1))
	  ;; Adjust str-width accordingly.
	  (setq str-width (- str-width 2)))

	;; Create an object representing the insertion.
	(when (eq token-kind 'symbol)
	  (let ((insertion))
	    ;; The start and end position needs to reflect the position of the
	    ;; insertion in the new query, as opposed to the token positions
	    ;; in the source string.
	    (setq insertion (make-instance
			     pgqa-query-string-insertion
			     :key str
			     :start pos-query
			     :end (+ pos-query str-width)))
	    ;; Addition to the end of the list is more expensive than addition
	    ;; to the beginning, but the insertions must be ordered by buffer
	    ;; position, see `pgqa-assign-insertions-to-nodes'.
	    (setq pgqa-sql-string-insertions
		  (append pgqa-sql-string-insertions (list insertion))))
	  )

	(setq query (concat query str)))
      (setq pos-query (+ pos-query str-width))
      (setq i (1+ i)))

    ;; Finally replace the string with the contained query.
    (atomic-change-group
      (setq end (- end nremoved))
      (delete-region start end)

      (save-excursion
	(goto-char start)
	(insert query))

      (dolist (insertion pgqa-sql-string-insertions)
	;; Now that we're done with changes of the buffer text, turn integer
	;; positions into markers (text replacement seems to break the
	;; contained markers).
	(pgqa-set-insertion-markers
	 insertion (nref insertion start) (nref insertion end)))

      ;; Set face of the insertions.
      (pgqa-set-sql-string-insertion-face pgqa-sql-string-insertions))
    )
  )

(defun pgqa-set-insertion-markers (insertion start end)
  "Add new start and end markers to insertion."

  (let ((m-start)
	(m-end))
    (setq m-start (make-marker))
    (set-marker m-start start)
    (nset insertion start m-start)

    (setq m-end (make-marker))
    (set-marker m-end end)
    (nset insertion end m-end)

    ;; User might want to edit the insertions before the query gets
    ;; parsed. The following settings should make it easier.
    (set-marker-insertion-type m-start nil)
    (set-marker-insertion-type m-end t))
  )

(defun pgqa-query-to-string (&optional indent)
  "Convert query to SQL string which can be used to execute the query from
PL/pgSQL procedure. If some parts of the query are marked as string variables
of the procedural language (i.e. insertions), these will appear separated from
other parts of the query parts by SQL concatenation operator."

  (interactive "P")

  ;; Most of these steps have the same rationale like in pgqa-format-query.
  (pgqa-check-customizations)
  (pgqa-parse t)

  (if (> (length pgqa-sql-string-insertions) 0)
      ;; Make nodes aware of insertions. The insertion positions will need to
      ;; be adjusted during the actual formatting.
      (pgqa-assign-insertions-to-nodes
       pgqa-query-tree
       (vector pgqa-sql-string-insertions)))

  ;; Turn the query into a string and quote it so it can be treated as an SQL
  ;; string.
  (pgqa-deparse indent t)

  ;; Cleanup.
  (pgqa-delete-sql-string-insertions))

(defun pgqa-create-insertion (start end)
  "Mark the part of the query within region as an insertion."

  (interactive "r")

  (let ((ins-start start)
	(ins-end end)
	(context)
	(matched)
	(nmatched)
	(ins-node)
	(markers)
	(node-start)
	(node-end)
	(insertion)
	(ins-new-key)
	(insertion-new)
	(insertions-reverse)
	(pushed))

    ;; Mark should now contain the new insertion, not the query. Remember and
    ;; and deactivate the mark so that parser does not think that the region
    ;; contains the query to be parsed.
    (deactivate-mark)

    ;; Make sure we have up-to-date query tree.
    (pgqa-parse-common nil)

    ;; The `matched' list is for output.
    (setq context (vector ins-start ins-end matched))

    (pgqa-walk pgqa-query-tree 'pgqa-find-insertion-candidates
	       context)

    ;; Insertion can only be created if exactly one node matches.
    (setq matched (elt context 2))
    (setq nmatched (length matched))
    (if (= nmatched 0)
	(user-error
	 "The region contains no potential insertion")
      (if (> nmatched 1)
	  (user-error
	   "The region contains more than one potential insertion"))
      )
    (setq ins-node (car matched))

    ;; Make sure that no existing insertion overlaps with the new one.
    (setq markers (nref ins-node markers))
    (setq node-start (elt markers 0))
    (setq node-end (elt markers 1))
    (dolist (insertion pgqa-sql-string-insertions)
      (let ((ins-start (nref insertion start))
	    (ins-end (nref insertion end)))
	(if (null
	     (or
	      (< node-end ins-start)
	      (> node-start ins-end)))
	    (user-error "The new insertion overlaps with existing one."))
	)
      ;; Since adding element to the list head is easier than adding it to the
      ;; end, use this iteration to construct a list ordered in the reverse
      ;; order. Thus we can also use `push' when adding the new insertion to
      ;; the list.
      (push insertion insertions-reverse))

    ;; Read insertion key and check it.
    (setq ins-new-key (read-from-minibuffer "Insertion key: "))
    (if (string-match "\\s-" ins-new-key)
	(user-error "The insertion key must not contain whitespace characters"))

    ;; Replace the original text with the key.
    (atomic-change-group
      ;; Temporarily change the marker insertion type so that it the inserted
      ;; text does not move it ahead.
      (cl-assert (marker-insertion-type node-start))
      (set-marker-insertion-type node-start nil)

      (delete-region node-start node-end)

      (save-excursion
	(goto-char node-start)
	(insert ins-new-key))

      ;; Restore the original insertion type.
      (set-marker-insertion-type node-start t))

    ;; Create the new insertion.
    (setq insertion-new (make-instance
			 pgqa-query-string-insertion
			 :key ins-new-key))

    ;; Assign it markers ...
    (pgqa-set-insertion-markers insertion-new
				node-start
				(+ node-start (string-width ins-new-key)))

    ;; ... and face.
    (pgqa-set-sql-string-insertion-face (list insertion-new))

    ;; Add it to the list of existing insertions. Make sure the insertion is
    ;; added to the correct position in the list.
    (setq pgqa-sql-string-insertions nil)
    (dolist (insertion insertions-reverse)
      (let ((ins-end (nref insertion end))
	    (ins-start-new (nref insertion-new start)))
	;; Given that ins-end is descending, the new insertion precedes the
	;; existing one if it's at higher position.
	(when (> ins-start-new ins-end)
	  (push insertion-new pgqa-sql-string-insertions)
	  (setq pushed t)))
      ;; Push the existing insertion.
      (push insertion pgqa-sql-string-insertions))

    ;; If the new insertion hasn't been pushed so far, it must be in front of
    ;; all the existing ones or it's the first one.
    (when (null pushed)
      (when (> (length insertions-reverse) 0)
	(setq insertion (car pgqa-sql-string-insertions))
	(cl-assert (< (nref insertion-new end) (nref insertion start))))

      (push insertion-new pgqa-sql-string-insertions))
    )
  )

(defun pgqa-find-insertion-candidates (node context)
  "Find nodes in the region that can be turned into SQL string insertion"

  (let* ((reg-start (elt context 0))
	 (reg-end (elt context 1))
	 (output (elt context 2))
	 (markers (nref node markers))
	 (node-start (elt markers 0))
	 (node-end (elt markers 1)))
    ;; Does the node fit into the region?
    (if (and (>= node-start reg-start) (<= node-end reg-end))
	;; Currently we create insertion for table name, column
	;; reference, numeric literal and string literal.
	(let ((cn (pgqa-object-class-name node)))
	  (when (or
		 (eq cn 'obj)
		 (eq cn 'number)
		 (eq cn 'string))
	    (push node output)
	    (aset context 2 output)))
      )
    )
  )

(defcustom pgqa-host "localhost"
  "Host name to which `pgqa-send-region' function connects."

  :type 'string)

(defcustom pgqa-port "5432"
  "TCP port to which `pgqa-send-region' function connects."

  :type 'string)

(defcustom pgqa-database "postgres"
  "Name of database to which `pgqa-send-region' function connects."

  :type 'string)

(defcustom pgqa-role "postgres"
  "Role that `pgqa-send-region' function uses to connect to database."

  :type 'string)

(defcustom pgqa-psql-path "psql"
  "Path to psql client application used by `pgqa-send-region' function."

  :type 'string)

(defcustom pgqa-psql-output "*pgqa-psql-output*"
  "Name of buffer to which `pgqa-send-region' writes the server response."

  :type 'string)

(defun pgqa-send-region (start end)
  "Send region to PostgreSQL server, connection to which is specified using
`pgqa-database' and `pgqa-role' customization variebles. The server response
is written to the buffer whose name is in `pgqa-psql-output'."

  (interactive "r")

  (get-buffer-create pgqa-psql-output)

  ;; Make sure the next message is written at the end of the buffer. XXX
  ;; Shouldn't the buffer be read-only?
  (with-current-buffer pgqa-psql-output
    (goto-char (point-max)))

  (call-process-region start end pgqa-psql-path  nil pgqa-psql-output nil
		       "-U" pgqa-role "-h" pgqa-host "-p" pgqa-port
		       pgqa-database)
  (deactivate-mark)

  ;; User should not need to scroll down manually.
  (with-current-buffer pgqa-psql-output
    (let ((w (get-buffer-window)))
      ;; Make sure the buffer is displayed.
      (unless w
	(setq w (next-window))
	(set-window-buffer w pgqa-psql-output))
      (select-window w)
      (goto-char (point-max))))
  )

(provide 'pgqa)
