;; Copyright (C) 2019 Antonin Houska
;;
;; This file is part of PGQA.
;;
;; PGQA is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; PGQA is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License qalong
;; with PGQA. If not, see <http://www.gnu.org/licenses/>.

;; Keep byte compiler quitet.
(defvar pgqa-class-field-name)
(defvar pgqa-class-field-parent)
(defvar pgqa-class-field-fields)
(defvar pgqa-class-field-defaults)
(defvar pgqa-class-field-methods)
(defvar pgqa-class-method-dump-raw)
(defvar pgqa-class-method-dump-node)
(defvar pgqa-class-method-walk)
(defvar pgqa-class-method-analyze)
(defvar pgqa-object-field-class)
(defvar pgqa-object-field-fields)

;; Make a new instance of class, and set its fields as requested.
(defun make-instance (class &rest fields)
  (let (
	;; String is the key, not symbol, therefore use `equal' for key
	;; comparisons.
	(values (make-hash-table :test 'equal))
	(nfields)
	(result (make-vector 2 nil))
	(defaults))

    ;; Process the initial values of the fields.
    (setq nfields (length fields))
    ;; The field specification consits of key-value pairs, so the number of
    ;; elements must be even.
    (unless (= nfields (* 2 (/ nfields 2)))
	(error (format
		"invalid field specification encountered for class '%s"
		class)))
    (if fields
	(let ((class-fields (aref class pgqa-class-field-fields))
	      (key-sym)
	      (key)
	      (value)
	      (key-valid))
	  (dotimes (i (/ nfields 2))
	    (setq key-sym (pop fields))
	    (setq value (pop fields))
	    (unless (keywordp key-sym)
	      (error (format "%s is not a keyword symbol" key-sym)))
	    (setq key (symbol-name key-sym))
	    (cl-assert (string= (substring key 0 1) ":") nil
		       "field name must start with colon")
	    ;; Get rid of the colon.
	    (setq key (substring key 1 nil))
	    (cl-assert (> (string-width key) 0)
		       "field name must not be empty")

	    ;; Is this a valid field of the class?
	    (setq key-valid (member key class-fields))
	    (unless key-valid
	      (error (format "class '%s does not have field '%s"
			     (aref class pgqa-class-field-name) key)))

	    ;; Set the field value.
	    (puthash key value values))
	  )
      )

    ;; Assign default values of fields not specified by caller.
    (setq defaults (aref class pgqa-class-field-defaults))
    (if defaults
	(maphash
	 (lambda (def-key def-value)
	   (if (null (gethash def-key values))
	       (puthash def-key def-value values))
	   )
	 defaults)
      )

    ;; Finalize the object.
    (aset result pgqa-object-field-class class)
    (aset result pgqa-object-field-fields values)
    result)
  )

(defmacro nref (obj field)
  `(let ((result))
     (setq result
	   ;; Quote the field name so that caller does not have to.
	   (gethash (symbol-name (quote ,field))
		    (aref ,obj pgqa-object-field-fields)))

     (if (null result)
	 (let* ((class (aref ,obj pgqa-object-field-class))
		(fields (aref class pgqa-class-field-fields)))
	   (if (null (member (symbol-name (quote ,field)) fields))
	       (error (format "Field '%s not found in class '%s"
			      (quote ,field)
			      (pgqa-object-class-name ,obj)))
	       )
	   )
       )
     result))

(defmacro nset (obj field value)
  `(let ((class)
	 (class-fields))
     (setq class (aref ,obj pgqa-object-field-class))
     (setq class-fields (aref class pgqa-class-field-fields))
     (unless (member (symbol-name (quote ,field)) class-fields)
       (error
	(format "'%s is not valid field of class '%s"
		(quote, field)
		(aref class pgqa-class-field-name))))

     (puthash (symbol-name (quote ,field)) ,value
	      (aref ,obj pgqa-object-field-fields))
     )
  )

(defun field-boundp (node field)
  (let ((fields (aref node pgqa-object-field-fields)))
    ;; TODO If the field is not there, throw error if the class does not
    ;; contain it at all.
    (gethash (symbol-name field) fields))
  )

;; Like eieio-object-class.
(defun pgqa-object-class (node)
  (aref node pgqa-object-field-class))

;; Like eieio-object-class-name.
(defun pgqa-object-class-name (node)
  (let ((class (aref node pgqa-object-field-class)))
    (aref class pgqa-class-field-name))
  )

;; Like EIEIO's object-of-class-p.
(defun pgqa-object-of-class-p (node class)
  (let ((obj-class (aref node pgqa-object-field-class))
	(i t)
	(match nil))

    (while i
      (if (null (eq (aref obj-class pgqa-class-field-name) class))
	  (setq obj-class (aref obj-class pgqa-class-field-parent))
	(setq match t)
	;; Done.
	(setq i nil))

      ;; No more parents?
      (if (null obj-class)
	  (setq i nil))
      )

    match)
  )

;; Like EIEIO's object-slots.
(defun pgqa-object-fields (obj)
  (let ((result))
    (maphash
     (lambda (key value)
       (push key result))
     (aref obj pgqa-object-field-fields))
    result)
  )

;; Like EIEIO's slot-value.
(defun pgqa-field-value (obj key)
  (gethash key
	   (aref obj pgqa-object-field-fields)))

;; Like EIEIO's set-slot-value.
(defun pgqa-set-field-value (obj field value)
  (puthash field
	   value
	   (aref obj pgqa-object-field-fields)))

(provide 'pgqa-node-api)
