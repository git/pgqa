;; Copyright (C) 2016 Antonin Houska
;;
;; This file is part of PGQA.
;;
;; PGQA is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; PGQA is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License qalong
;; with PGQA. If not, see <http://www.gnu.org/licenses/>.

;; This is a minimalistic implementation of classes and objects.
;;
;; Initially PGQA used the EIEIO Emacs library, however that didn't appear to
;; be great. First, it did not show call stack if exception occurred in class
;; method. Second, if a function calli `make-instance' had an argument of the
;; same name as a class field, warning that the variable is obsolete was
;; produed during byte compilation.

;; The class hierarchy.
;;
;; Each class is represented by a list whose items have the following meaning:
;;
;;	1) class name
;;
;;	2) parent class name
;;
;;	3) list of fields, where each item is either a string (field name) or
;;	a 2 element list (field name, default value)
;;
;;	4) list of methods, where the order is identical to the order of
;;	methods in preprocessed class, see below.

(require 'pgqa-analyze)
(require 'pgqa-node-impl)
(require 'pgqa-dump-raw)

(eval-when-compile
  (require 'pgqa-node-api))

;; `pgqa-class-hash' contains the class hierarchy preprocessed so that the
;; classes can be instantiated.
;;
;; Unlike `pgqa-class-list', this is a hash table where class name is the
;; key. Each item is a vector, so that elements can be accessed
;; efficiently. The vector items are as follows:
;;
;;	1) class name
;;
;;	2) parent class (i.e. object, not just name).
;;
;;	3) list of fields of the class itself plus fields of parent
;;	classes. XXX Consider sorting so that make-instance runs faster.
;;
;;	4) hash of field default values (field name is the key, the default
;;	value is the value). The hash only contains fields which do have the
;;	default value. Default value of the field in child class overrides the
;;	default value of the parent class
;;
;;	5) vector of class methods for descendants of node class, nil
;;	otherwise (for auxiliary classes).

;; Indices of class fields.
(defconst pgqa-class-field-name 0)
(defconst pgqa-class-field-parent 1)
(defconst pgqa-class-field-fields 2)
(defconst pgqa-class-field-defaults 3)
(defconst pgqa-class-field-methods 4)

;; Indexes of methods in the containing vector.
(defconst pgqa-class-method-dump-raw 0)
(defconst pgqa-class-method-dump-node 1)
(defconst pgqa-class-method-walk 2)
(defconst pgqa-class-method-analyze 3)

;; Find function by name and put it to the appropriate slot of vector.
(defun pgqa-assign-class-method (prefix classname meth-vec index)
  (let ((meth-str (format "%s-%s" prefix classname)))
    (aset meth-vec index (intern-soft meth-str))
    )
  )

;; If (node) class does not have given method, try to find it in the nearest
;; ancestor class.
(defun pgqa-assing-parent-class-method (class meth-vec index)
  (let ((meth (aref meth-vec index))
	(class-parent class)
	(meths-parent)
	(meth-parent)
	(i t))
    (unless meth
      (while i
	(setq class-parent (aref class-parent pgqa-class-field-parent))
	(if (null class-parent)
	    (setq i nil)
	  (setq meths-parent (aref class-parent pgqa-class-field-methods))
	  ;; The parent should also be a node descendant, so it should the
	  ;; methods vector, even though possibly empty.
	  (cl-assert meths-parent
		     nil "parent class must have methods")
	  (when meths-parent
	    (setq meth-parent (aref meths-parent index))
	    (when meth-parent
	      (aset meth-vec index meth-parent)
	      ;; Done
	      (setq i nil)))
	    )
	  )
	)
    )
  )

;; Turn `pgqa-class-list' into `pgqa-class-hash'.
(defun pgqa-preprocess-classes ()
  (let ((classes-raw (make-hash-table))
	(result (make-hash-table))
	(classname)
	(class)
	(parent-name)
	(parent-raw)
	(parents-raw)
	(parent)
	(attrhash (make-hash-table))
	(defhash (make-hash-table)))

    (dolist (class-raw pgqa-class-list)
      (setq classname (car class-raw))

      ;; Check for duplicate class names.
      (if (gethash classname classes-raw)
	  (error (format "Class name %s is duplicate" classname)))

      ;; Preprocessing requires repeated access to the class definitions, so
      ;; first of all put all the definitions into a temporary hash table.
      (puthash classname class-raw classes-raw)

      ;; Create vector representing the new class and add it to result
      ;; hash. The fields other than name will be initialized later, but we
      ;; need the vector itself early so that we can assign parents to
      ;; classes.
      (setq class (make-vector 5 nil))
      (aset class pgqa-class-field-name classname)
      (puthash classname class result))

    ;; Now do the actual preprocessing.
    ;;
    ;; First, identify the (direct) parent class and fields defined for the
    ;; classes directly.
    (dolist (class-raw pgqa-class-list)
      (setq classname (car class-raw))

      (setq parent-name (car (cdr class-raw)))
      (when parent-name
	(setq parent (gethash parent-name result))
	(if (null parent)
	    (error (format "Parent class '%s' not found" parent-name)))
	(setq class (gethash classname result))

	;; Assign the parent.
	(aset class pgqa-class-field-parent parent))

      ;; Gather attributes and default values.
      (let ((attlist (nth 2 class-raw))
	    (attname)
	    (defval)
	    (has-defval)
	    (attnames)
	    (attdefaults (make-hash-table)))
	(when attlist
	  (dolist (att attlist)
	    (if (listp att)
		(progn
		  (setq attname (car att))
		  (setq defval (cdr att))
		  (setq has-defval t))
	      (setq attname att)
	      (setq has-defval nil))
	    (push (symbol-name attname) attnames)
	    (if has-defval
		(puthash attname defval attdefaults))
	    )

	  ;; Store the results in the class.
	  (setq class (gethash classname result))
	  (if (> (length attnames) 0)
	      (aset class pgqa-class-field-fields attnames))
	  (if (> (hash-table-count attdefaults) 0)
	      (aset class pgqa-class-field-defaults attdefaults)))
	)
      )

    ;; Now that each class has the parent assigned, we can gather inherited
    ;; fields and default values.
    (dolist (class-raw pgqa-class-list)
      (setq classname (car class-raw))
      (setq class (gethash classname result))
      (let ((chain)
	    (attrs)
	    (attrs-all)
	    (defs)
	    (defs-all))

	;; Create a chain of classes which starts with the current class and
	;; continues with its direct parent, followed by the more distant
	;; ancestors.
	(push class chain)
	(while (setq parent (aref class pgqa-class-field-parent))
	  (push parent chain)
	  (setq class parent))

	;; Gather attributes the class as well as those of all the ancestor
	;; classes. XXX Check if the same attribute is defined by multiple
	;; classes in the chain?
	(dolist (class chain)
	  (setq attrs (aref class pgqa-class-field-fields))
	  (setq attrs-all (seq-concatenate 'list attrs-all attrs)))

	;; Store the attribute list in a separate hash so far. We cannot
	;; modify the current class yet because it can be parent of another
	;; class that we haven't processed yet.
	(puthash classname attrs-all attrhash)

	;; Likewise, gather default values.
	(dolist (class chain)
	  (setq defs (aref class pgqa-class-field-defaults))
	  (when defs
	    ;; If the hash table to collect all defaults of this class does
	    ;; not exist yet, create it.
	    (if (null (gethash classname defhash))
		(puthash classname
			 ;; String is the key, not symbol, therefore use
			 ;; `equal' for key comparisons.
			 (make-hash-table :test 'equal)
			 defhash))
	    (let ((defs-this (gethash classname defhash)))
	      ;; Copy all the default values from `defs' to `defs-this'.
	      (maphash (lambda (key value)
			 (puthash (symbol-name key) value defs-this))
		       defs))
	    )
	  )
	)
      )

    ;; Assign the new attribute lists, as well as the hash tables of default
    ;; values to classes.
    (dolist (class-raw pgqa-class-list)
      (setq classname (car class-raw))
      (setq class (gethash classname result))
      (aset class pgqa-class-field-fields (gethash classname attrhash))

      ;; Default values are not always there.
      (let ((defs (gethash classname defhash)))
	(if defs
	    (aset class pgqa-class-field-defaults defs)))
      )

    ;; Create vectors of class methods.
    (dolist (class-raw pgqa-class-list)
      (setq classname (car class-raw))
      (setq class (gethash classname result))

      (let ((found)
	    (methods)
	    (i t))

	;; Find out if this class is a descendant of the "node" class.
	(while (and i class)
	  (if (equal (aref class pgqa-class-field-name) 'node)
	      (progn
		(setq found t)
		(setq i nil))
	    (setq class (aref class pgqa-class-field-parent)))
	  )

	(when found
	  ;; Create vector of methods.
	  (setq methods (make-vector 4 nil))

	  ;; Assign the symbols to the vector slots. At this stage we don't
	  ;; check parent classes because they are not guaranteed to have
	  ;; methods assigned themselves.
	  (pgqa-assign-class-method "pgqa-dump-raw" classname methods
				    pgqa-class-method-dump-raw)
	  (pgqa-assign-class-method "pgqa-dump-node" classname methods
				    pgqa-class-method-dump-node)
	  (pgqa-assign-class-method "pgqa-walk" classname methods
				    pgqa-class-method-walk)
	  (pgqa-assign-class-method "pgqa-analyze" classname methods
				    pgqa-class-method-analyze)

	  (aset (gethash classname result) pgqa-class-field-methods methods))
	)
      )

    ;; Now that all the methods should be assigned, check which classes miss
    ;; any methods and look for them in parent classes.
    (dolist (class-raw pgqa-class-list)
      (setq classname (car class-raw))
      (setq class (gethash classname result))

      (let ((methods (aref class pgqa-class-field-methods)))
	;; If the vector is not there, it should be an "auxiliary class", not
	;; a query tree node.
	(when methods
	  (pgqa-assing-parent-class-method class methods
					   pgqa-class-method-dump-raw)
	  (pgqa-assing-parent-class-method class methods
					   pgqa-class-method-dump-node)
	  (pgqa-assing-parent-class-method class methods
					   pgqa-class-method-walk)
	  (pgqa-assing-parent-class-method class methods
					   pgqa-class-method-analyze))
	)
      )

    (dolist (class-raw pgqa-class-list)
      (setq classname (car class-raw))
      (setq class (gethash classname result))

      ;; Create a symbol for the class so it can be accessed later. The "pgqa"
      ;; prefix is added to reduce risk of conflict with other symbols.
      (let ((class-sym))
	(setq class-sym (intern (format "pgqa-%s" classname))) ;
	;; The class itself is the symbol value.
	(set class-sym class))
      )

    result)
  )

(defvar pgqa-class-hash
  (pgqa-preprocess-classes))


;; An object is a vector of the following items:
;;
;;	1) class reference
;;
;;	2) hash of field values. (Field that has no default value must have
;;	value assigned at object creation time.)
(defconst pgqa-object-field-class 0)
(defconst pgqa-object-field-fields 1)

;; Keep byte compiler quiet.
(defvar pgqa-gui-node)

;; TODO Generate function like this automatically for each class.
(defun pgqa-gui-node (&rest fields)
  (apply 'make-instance pgqa-gui-node fields))

(provide 'pgqa-node)
