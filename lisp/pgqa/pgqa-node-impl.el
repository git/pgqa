;; Copyright (C) 2019 Antonin Houska
;;
;; This file is part of PGQA.
;;
;; PGQA is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; PGQA is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License qalong
;; with PGQA. If not, see <http://www.gnu.org/licenses/>.

(eval-when-compile
  (require 'pgqa-node-api))

;; Keep byte compiler quitet.
(defvar pgqa-class-field-name)
(defvar pgqa-class-field-parent)
(defvar pgqa-class-field-fields)
(defvar pgqa-class-field-defaults)
(defvar pgqa-class-field-methods)
(defvar pgqa-class-method-dump-raw)
(defvar pgqa-class-method-dump-node)
(defvar pgqa-class-method-walk)
(defvar pgqa-class-method-analyze)
(defvar pgqa-object-field-class)
(defvar pgqa-object-field-fields)
(defvar pgqa-query-markers)
(defvar pgqa-query)

(defvar pgqa-class-list
  '(
    ;; Class representing a generic node of SQL query tree.
    (node
     nil
     (;; Start and end position of a nonterminal.
      region
      ;; Start and end position in the form of a marker.
      markers
      ;; An instance of `query-string-insertion' if assigned.
      (insertion . nil))
     nil
     )

    ;; Special node to carry information on region and markers of an operator
    ;; string. As it's a subclass of pgqa-node, it can be processed by
    ;; pgqa-setup-node-gui.
    (gui-node
     node
     (
      ;; Currently 'operator or 'func-call. Both for the sake of highlighting.
      parent-kind
      )
     )

    ;; Node representing an operation on one or multiple arguments.
    (expr
     node
     (
      args
      )
     )

    ;; SQL query (or a subquery)
    ;;
    ;; CAUTION! Keep `pgqa-copy-common-query-fields' up-to-date if adding new
    ;; slots.
    (query
     node
     (;; SELECT, INSERT, UPDATE, DELETE
      kind

      (target-expr . nil)

      ;; SELECT ... INTO ...
      (into-expr . nil)

      (from-expr . nil)

      ;; An instance of `group-having-sort-part' class. Only used at parse
      ;; time, then converted to group-by, having and order-by below.
      (group-having-sort-part . nil)

      ;; The parts `group-having-sort-part' gets flattened into.
      (group-by . nil)
      (having . nil)
      (order-by . nil)

      ;; A list used temporarily when parsing an INSERT query. The first
      ;; element is table name (eventually to be assigned to target-table'
      ;; slot). If the list has 2nd element, it's a list of columns
      ;; (eventually to be assigned to insert-cols slot).
      (insert-table-expr . nil)

      ;; Table subject to INSERT / UPDATE / DELETE.
      (target-table . nil)

      ;; Column list if the query is INSERT command.
      (insert-cols . nil)

      ;; Either SELECT query or VALUES clause of INSERT command.
      (insert-input . nil)
      )
     )

    ;; FROM expression of an SQL query.
    (from-expr
     node
     (from-list
      qual
      )
     )

    ;; A single argument represents table, join, function, subquery or VALUES
    ;; clause. If the 'args slot has elements, the FROM list entry is a join.
    (from-list-entry
     expr
     (
      ;; Instance of from-list-entry-alias.
      alias

      ;; Is the entry prepended by LATERAL keyword?
      (lateral . nil)

      ;; For a simple entry, the value is one of "table", "function", "query",
      ;; "values". For join it's "left", "right", "full" (nil implies inner
      ;; join as long as 'args has 2 elements).
      kind

      ;; Join expression if the entry is a join.
      qual
      )
     )

    ;; Generic alias.
    (alias
     node
     (name
      )
     )

    ;; Alias output column.
    (alias-arg
     node
     (
      var

      ;; An instance of `data-type'.
      datatype
      )
     )

    (from-list-entry-alias
     node
     (
      ;; Instance of `pgqa-alias'.
      name

      ;; If bound, it's an instance of `node-list' of `alias-arg' items.
      cols
      )
     )

    ;; This class exists primarily for pgqa-dump. Top-level clauses that contain a
    ;; single expression should be represented by its subclass.
    (top-clause
     node
     (expr
      )
     )

    ;; GROUP BY clause
    (group-clause
     top-clause
     (
      )
     )

    ;; HAVING clause
    ;;
    ;; XXX Shouldn't `group-clause' be just renamed to be more generic and
    ;; used here as well?
    (having-clause
     top-clause
     (
      )
     )

    ;; ORDER BY clause.
    (sort-clause
     top-clause
     (
      )
     )

    ;; Wrapper for GROUP BY and HAVING. Since we have a single rule for both
    ;; (in order to keep the number of rules for the query reasonably low), it
    ;; makes sense to have both clauses in a single structure. (List could do
    ;; the work but class makes tree walker(s) easier to read.)
    ;;
    ;; This class is only used at parse time. Once parsing is complete, it
    ;; gets flattened.
    ;;
    ;; As the contained clauses do have their GUI nodes, the only reason for
    ;; this class to inherit from `node' is that it has to pass region info to
    ;; the containing query in the generic way.
    (group-having-part
     node
     (
      ;; Instance of `group-clause'.
      (group . nil)
      ;; Instance of `having-clause'.
      (having . nil)
      )
     )

    (group-having-sort-part
     node
     (
      ;; Instance of `group-having-part'.
      (group-having . nil)

      ;; Instance of `sort-clause'.
      (sort . nil)
      )
     )

    ;; Wrapper for "SELECT" and "INTO" clauses. Like above, this only helps to
    ;; reduce the number of parser rules, and is flattened at some point.
    (select-into-part
     node
     ;; Instance of pgqa-target-list.
     (select

      (into . nil)
      )
     )

    ;; Function call.
    (func-call
     node
     (
      name

      ;; arguments are stored as a single pgqa-operator having :op=",".
      args

      ;; Like `operator'. XXX Is it worth to define a common parent class for
      ;; pgqa-func-call, pgqa-operator and possibly some other classes that
      ;; might be highlighted in the future?
      (gui . nil)
      )
     )

    ;; String constant
    (string
     node
     (
      value
      )
     )

    ;; Numeric constant.
    ;;
    ;; Number is currently stored as a string - should this be changed?
    (number
     node
     (
      value
      )
     )

    ;; Table or column reference.
    (obj
     expr
     (
      ;; The `args' field (inherited from pgqa-expr) contains the
      ;; dot-separated components of table / column reference.
      ;;
      ;; XXX Can't we simply use pgqa-expr class here?
      ;;
      ;; x.y expression can represent either column "y" of table "x" or table
      ;; "y" of schema "x". Instead of teaching parser to recognize the
      ;; context (is it possible?) let's postpone resolution till analysis
      ;; phase.
      ;;
      ;; Note that the number of arguments is not checked during "raw
      ;; parsing", and that asterisk can be at any position, not only the last
      ;; one.
      )
     )

    ;; Data type name.
    (data-type
     expr
     (
      ;; The `args' field (inherited from pgqa-expr) contains a list of type
      ;; name components, each of which should be an instance of pgqa-obj. In
      ;; most cases it's a single-item list, but can have 2 items at least in
      ;; one special case ("double precision").

      ;; Number of array dimensions. 0 if the data type is not an array.
      (dims . 0)
      )
     )

    ;; CASE expression.
    (case
     node
     (
      ;; The expression following the CASE keyword.
      arg

      ;; A list of `case-branch' instances.
      branches

      ;; The ELSE expression.
      else
      )
     )

    ;; The "WHEN ... THEN ..." part of CASE expression.
    (case-branch
     expr
     (
      ;; The (inherited) `args' field contains a 2-element list. The first and
      ;; second element represent arguments of the WHEN and THEN clause
      ;; respectively.
      )
     )

    ;; Generic operator.
    (operator
     expr
     (
      ;; TODO Rename to 'code ?
      op

      ;; Region info and marker of the operator string(s) is stored separate
      ;; so that access to the string remains straightforward. Can be a single
      ;; instance of gui-node or list of those.
      (gui . nil)

      ;; Operator precedence, for the sake of printing.
      prec

      ;; If the expression has only one argument, it's considered to be an
      ;; unary operator. This slot tells whether it's a postfix operator. nil
      ;; indicates it's a prefix operator.
      (postfix . nil)
      )
     )

    ;; Sublink is mostly treated like an operator, but it seems to deserve
    ;; separate class. The :op slot indicates the sublink kind (e.g. EXISTS),
    ;; and subquery is the only argument.
    ;;
    ;; TODO Simplify instance creation so that the subquery can be passed
    ;; directly instead of being wrapped in a singl-element list.

    (sublink
     operator
     (
      )
     )

    ;; List of nodes.
    ;;
    ;; Some nodes are actually plain lists, but it's easier to work with a
    ;; class because it has a name.
    (node-list
     operator
     (
      (op . ",")

      (prec . pgqa-precedence-comma)
      )
     )

    ;; Query target list.
    (target-list
     node-list
     (
      )
     )

    ;; Target list entry.
    (target-entry
     node
     (
      expr

      alias
      )
     )

    ;; Metadata to control deparsing of an SQL query and the contained
    ;; expressions.
    ;;
    ;; Note: the simplest way to control (not) breaking line in front of a
    ;; subquery is to create a separate instance of this class for the
    ;; subquery.
    ;;
    ;; XXX If adding a new field, check if it's subject to backup / restore by
    ;; pgqa-dump method of pgqa-operator class. (Is this backup / restore
    ;; worth separate functions?)
    ;;
    ;; TODO Rename to dump-state ?
    (deparse-state
     nil
     (
      ;; Indentation level of the top level query.
      ;;
      ;; This is the indentation passed to `pgqa-dump-node',
      ;; `pgqa-deparse-string' and subroutines is relative to this.
      indent

      ;; Indentation, relative to `indent' above, of top level expression,
      ;; e.g. the text following top-level keyword. See
      ;; `pgqa-deparse-top-keyword' for details.
      indent-top-expr

      ;; Column at which the following node (or its leading space) should
      ;; start.
      next-column

      ;; The width of the next space to be printed out.
      next-space

      ;; Is the current line empty or contains only whitespace?
      line-empty

      ;; Position in the output buffer at which the next string will start.
      buffer-pos

      ;; String to which each node appends its textual representation.
      result
      )
     )

    ;; If the input query has the form of SQL string, it can consist of parts
    ;; concatenated using the SQL concatenation operator (||). Each part is
    ;; either an SQL string or an insertion. For example, if PL/pgSQL language
    ;; is used to construct the query string, then PL/pgSQL variable
    ;; representing part of the string is the insertion.
    (query-string-insertion
     nil
     (
         ;; The inserted string is used as a key. XXX There can be multiple
      ;; insertions with the same key. Should this slot be renamed or should
      ;; the insertions be organized in a different way than a single list
      ;; (e.g. a hash containing a list for each key)?
      key

      ;; Where in the buffer the insertion starts and ends.
      start

      end

      (overlay . nil)
      )
     )

    ;; Input / output for `pgqa-dump-maybe-insertion'.
    (string-to-query-context
     nil
     (
      (last-insertion . nil)
      )
     )

    ;; Metadata to control printing out the internal representation of a query
    ;; tree node.
    (dump-raw-state
     nil
     (
      ;; Function that returns the initial part of the node. The funcion
      ;; receives the node and indentation level as the first and second
      ;; arguments respectively.
      node-start

      ;; Function that returns the final part of the node. The funcion
      ;; receives the node and indentation level as the first and second
      ;; arguments respectively.
      node-end

      ;; String to which each node appends its textual representation.
      result)
     )

    ;; A problem identified during query analysis.
    (problem
     nil

     (
      ;; Message that user should see
      message

      ;; Integer (marker) position the problem was found at, or a vector
      ;; containing both start and end position.
      location)
     )

    ;; Information passed across query tree nodes during analysis.
    (analyze-context
     nil

     (
      ;; List of errors / warnings encountered during analysis.
      (problems . nil))
     )
    )
  )

(defun pgqa-walk-node (node walker context)
  (funcall walker node context))

;; gui can be either a single instance of pgqa-gui-node or a list of them.
;;
;; TODO Rename, so it does not look like a walker of `gui' class (which does
;; not exist now, but ...)
(defun pgqa-walk-gui (node walker context)
  (let ((gui (nref node gui)))
    (if (listp gui)
	(pgqa-node-walk-list gui walker context)
      (funcall walker gui context)))
  )

;; Besides attaching the markers to nodes, add them to pgqa-query-markers, to
;; make cleanup easier.
;;
;; context is currently used to indicate that the regions have been
;; initialized during a dump, which can't easily avoid leading whitespace. So
;; if the value is non-nil, we should skip that whitespace when setting up the
;; marker.
(defun pgqa-setup-node-gui (node context)
  "Turn region(s) into a markers to the node."
  (let* ((reg-vec (nref node region))
	 (reg-start (elt reg-vec 0))
	 (reg-end (elt reg-vec 1))
	 (m-start (make-marker))
	 (m-end (make-marker))
	 (o))

    ;; If the region might start with a whitespace, skip it (the whitespace).
    (when context
      (goto-char reg-start)
      (while (and (looking-at "\\s-\\|\$")
		  (< reg-start (point-max)))
	(setq reg-start (1+ reg-start))
	(goto-char reg-start)))

    (set-marker m-start reg-start)
    (set-marker m-end reg-end)

    ;; The insertion types are such that the start and end markers always span
    ;; only the original region.
    (set-marker-insertion-type m-start t)
    (set-marker-insertion-type m-end nil)

    ;; Keep track of markers.
    (push m-start pgqa-query-markers)
    (push m-end pgqa-query-markers)

    (nset node markers (vector m-start m-end)))
  )

;; For performance reasons (see the Overlays section of Elisp documentation)
;; we assign the face as text property, although cleanup would be simpler if
;; we assigned the face via overlay.
(defun pgqa-set-node-face (node context)
  (when (eq (pgqa-object-class-name node) 'gui-node)
    (when (field-boundp node 'markers)
      (let* ((m (nref node markers))
	     (m-start (elt m 0))
	     (m-end (elt m 1))
	     (kind (nref node parent-kind))
	     (face))

	(if (eq kind 'operator)
	    (setq face 'pgqa-operator-face)
	  (if (eq kind 'func-call)
	      (setq face 'pgqa-func-call-face)
	    (error
	     (format
	      "Unrecognized kind of gui node parent: %s"
	      kind))))
	(put-text-property m-start m-end
			   'font-lock-face face))
      )
    )
  )

;; Remove the face added previously by pgqa-set-node-face.
(defun pgqa-reset-node-face (node context)
  (if (eq (pgqa-object-class-name node) 'gui-node)
      (if (field-boundp node 'markers)
	  (let* ((m (nref node markers))
		 (m-start (elt m 0))
		 (m-end (elt m 1)))
	    (remove-text-properties m-start m-end
				    '(font-lock-face nil)))
	)
    )
  )

;; An utility to apply a function to all nodes of a tree.
;;
;; If the walker function changes the nodes, caller is responsible for having
;; taken a copy of the original node.
;;
;; Currently it seems more useful to process the sub-nodes before the actual
;; node.
(defun pgqa-walk (node walker context)
  (let* ((class (aref node pgqa-object-field-class))
	 (methods (aref class pgqa-class-field-methods))
	 (method (aref methods pgqa-class-method-walk)))
    (funcall method node walker context))
  )

(defun pgqa-node-walk-list (node-list walker context)
  "Run the node walker function on each item of a list."

  (dolist (node node-list)
    (pgqa-walk node walker context)))

(defun pgqa-copy-common-query-fields (dst src)
  (nset dst target-expr (nref src target-expr))
  (if (nref src into-expr)
      (nset dst into-expr (nref src into-expr)))
  (if (nref src from-expr)
      (nset dst from-expr (nref src from-expr)))
  (if (nref src group-by)
      (nset dst group-by (nref src group-by)))
  (if (nref src having)
      (nset dst having (nref src having)))
  (if (nref src order-by)
      (nset dst order-by (nref src order-by))))

(defun pgqa-walk-query (node walker context)
  (if (nref node target-expr)
      (pgqa-walk (nref node target-expr) walker context))

  (if (nref node into-expr)
      (pgqa-walk (nref node into-expr) walker context))

  (if (nref node target-table)
      (pgqa-walk (nref node target-table) walker context))

  (if (nref node insert-input)
      (pgqa-walk (nref node insert-input) walker context))

  (if (nref node from-expr)
      (let ((fe (nref node from-expr)))
	(if (nref fe from-list)
	    (pgqa-node-walk-list (nref fe from-list) walker context))
	(if (field-boundp fe 'qual)
	    (pgqa-walk (nref fe qual) walker context))))

  (if (nref node group-by)
    (pgqa-walk (nref node group-by) walker context))
  (if (nref node having)
    (pgqa-walk (nref node having) walker context))
  (if (nref node order-by)
    (pgqa-walk (nref node order-by) walker context))

  (funcall walker node context))

(defun pgqa-walk-from-list-entry (node walker context)
  (if (field-boundp node 'alias)
      (funcall walker (nref node alias) context))

  ;; If node is a join, recurse into the sides and process qualifier.
  (let ((args (nref node args)))
    (if (= (length args) 2)
	(progn
	  (pgqa-walk (car args) walker context)
	  (pgqa-walk (car (cdr args)) walker context)
	  (pgqa-walk (nref node qual) walker context))
      ;; Process the single argument (query, function, ...).
      (cl-assert (= (length args) 1) nil
		 "from-list entry has incorrect number of arguments")
      (pgqa-walk (car args) walker context))
    (funcall walker node context)))

(defun pgqa-walk-top-clause (node walker context)
  (pgqa-walk (nref node expr) walker context))

;; Flatten the parse-time classes like `pgqa-group-having-sort-part'.
(defun pgqa-flatten-query-tree (node context)
  (if (eq (pgqa-object-class node) pgqa-query)
      (let ((ghs (nref node group-having-sort-part))
	    (gh)
	    (kind (nref node kind)))
	(when ghs
	  (setq gh (nref ghs group-having))
	  (when gh
	    (nset node group-by (nref gh group))
	    (nset node having (nref gh having)))
	  (nset node order-by (nref ghs sort))

	  ;; The slot is no longer needed.
	  (nset node group-having-sort-part nil))

	(when (string= kind "INSERT")
	  (let ((input (nref node insert-input))
		(ite (nref node insert-table-expr)))

	    ;; Copy the actual slots.
	    (pgqa-copy-common-query-fields node input)

	    (nset node target-table (car ite))
	    (if (cdr ite)
		(nset node insert-cols (nth 1 ite))))
	  )
	)
    )
  )

(defun pgqa-walk-func-call (node walker context)
  (funcall walker (nref node name) context)
  (if (nref node args)
      (pgqa-walk (nref node args) walker context))

  (if (nref node gui)
    (pgqa-walk-gui node walker context))
  (funcall walker node context))

(defun pgqa-walk-string (node walker context)
  (funcall walker node context))

(defun pgqa-walk-number (node walker context)
  (funcall walker node context))

(defun pgqa-walk-obj (node walker context)
  ;; The individual args are strings, so only process the alias.
  (funcall walker node context))

(defun pgqa-walk-data-type (node walker context)
  (funcall walker node context))


(defun pgqa-walk-case (node walker context)
  (pgqa-node-walk-list (nref node branches) walker context)
  (funcall walker node context))

(defun pgqa-walk-case-branch (node walker context)
  (pgqa-node-walk-list (nref node args) walker context)
  (funcall walker node context))

(defun pgqa-walk-operator (node walker context)
  (pgqa-node-walk-list (nref node args) walker context)

  (if (nref node gui)
      (pgqa-walk-gui node walker context))
  (funcall walker node context))

(defun pgqa-walk-target-entry (node walker context)
  (pgqa-walk (nref node expr) walker context)
  (funcall walker node context))

(provide 'pgqa-node-impl)
