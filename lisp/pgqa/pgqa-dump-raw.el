;; Copyright (C) 2016 Antonin Houska
;;
;; This file is part of PGQA.
;;
;; PGQA is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; PGQA is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License qalong
;; with PGQA. If not, see <http://www.gnu.org/licenses/>.

;; This module prints the query tree in machine readable format.

(eval-when-compile
  (require 'subr-x)
  (require 'pgqa-node-api))

;; Keep byte compiler quitet.
(defvar pgqa-class-field-name)
(defvar pgqa-class-field-parent)
(defvar pgqa-class-field-fields)
(defvar pgqa-class-field-defaults)
(defvar pgqa-class-field-methods)
(defvar pgqa-class-method-dump-raw)
(defvar pgqa-class-method-dump-node)
(defvar pgqa-class-method-walk)
(defvar pgqa-class-method-analyze)
(defvar pgqa-object-field-class)
(defvar pgqa-object-field-fields)

(defcustom pgqa-query-tree-print-region t
  "If non-nil, the output of `pgqa-dump-raw' contains node region info."

  :type 'boolean
  :group 'pgqa)

;; The entry point to call pgqa-dump-raw-... object method.
(defun pgqa-dump-raw (node state indent)
  (let* ((class (aref node pgqa-object-field-class))
	 (methods (aref class pgqa-class-field-methods))
	 (method (aref methods pgqa-class-method-dump-raw)))
    (funcall method node state indent))
  )

(defun pgqa-dump-raw-append (state str)
  "Append STR to the current result."

  (nset state result (format "%s%s" (nref state result) str)))

;; Convenience macro to call `node-start' or `node-end' function of the
;; `state' and update the `result' accordingly.
;;
;; XXX Think of better name.
(defmacro pgqa-dump-raw-boundary (kind node state indent)
  `(let ((func (nref state ,kind))
	 (orig (nref ,state result)))
     (nset ,state result
	   (string-join
	    (list
	     orig
	     (funcall func ,node ,indent))))))

(defun pgqa-node-to-lisp-start (node indent)
  "Turn the query tree node into the initial part of lisp-like expression."

  (let ((region "")
	(node-name)
	(indent-str (pgqa-lisp-indent-string indent)))
    (setq node-name (symbol-name (pgqa-object-class-name node)))

    (if pgqa-query-tree-print-region
	(setq region (format " %s" (nref node region))))
    (format "%s(%s%s\n" indent-str node-name region)))

(defun pgqa-node-to-lisp-end (node indent)
  "Turn the query tree node slot into the final part of lisp-like expression."

  (format "%s)\n" (pgqa-lisp-indent-string (1+ indent))))

;; Subroutine for the macros below.
(defun pgqa-slot-to-lisp-common (node state slot-name indent)
  (nset state result
	(format "%s%s:%s"
		(nref state result)
		(pgqa-lisp-indent-string indent)
		slot-name))
  )

(defmacro pgqa-simple-slot-to-lisp (node state slot-name indent)
  "Turn a simple slot of the query tree node into a string."

  `(progn
     (pgqa-slot-to-lisp-common
      ,node
      ,state
      (quote ,slot-name)
      ,indent)

     (pgqa-dump-raw-append ,state
			   (format " %s\n"
				   (nref ,node ,slot-name)))
     )
  )

;; Like above but the slot is a node itself.
(defmacro pgqa-node-slot-to-lisp (node state slot-name indent)
  "Turn a node slot of the query tree node into a string."

  `(progn
     (pgqa-slot-to-lisp-common
      ,node
      ,state
      (format "%s%s" (quote ,slot-name) "\n")
      ,indent)

     (pgqa-dump-raw (nref ,node ,slot-name) ,state (1+ ,indent)))
  )

;; Like above but the slot is a list.
(defmacro pgqa-list-slot-to-lisp (node state slot-name indent)
  "Turn a list slot of the query tree node into a string."

  `(progn
     (pgqa-slot-to-lisp-common
      ,node
      ,state
      (format "%s%s" (quote ,slot-name) "\n")
      ,indent)

     (pgqa-dump-raw-append ,state
			   (format "%s(\n"
				   (pgqa-lisp-indent-string (1+ ,indent))))

     (dolist (i (nref ,node ,slot-name))
       (pgqa-dump-raw i state (+ ,indent 2)))

     (pgqa-dump-raw-append ,state
			   (format "%s)\n"
				   (pgqa-lisp-indent-string (+ ,indent 2))))
     )
  )

;; Create string for given indentation level, for the lisp output.
(defun pgqa-lisp-indent-string (indent)
  (make-string (* indent 2) 32))

;; `state' is an instance of `pgqa-dump-raw-state' class.
;;
;; `indent' is the number of ancestor nodes.
(defun pgqa-dump-raw-node (node state indent)
  "Print out the internal representation of a query node."

  (pgqa-dump-raw-boundary node-start node state indent)
  (pgqa-dump-raw-boundary node-end node state indent))


(defun pgqa-dump-raw-query (node state indent)
  "Print out the internal representation of a query."

  (pgqa-dump-raw-boundary node-start node state indent)

  (pgqa-node-slot-to-lisp node state target-expr (1+ indent))
  (if (nref node into-expr)
      (pgqa-node-slot-to-lisp node state into-expr (1+ indent)))

  (if (nref node from-expr)
      (pgqa-node-slot-to-lisp node state from-expr (1+ indent)))

  (if (nref node group-by)
      (pgqa-node-slot-to-lisp node state group-by (1+ indent)))
  (if (nref node having)
      (pgqa-node-slot-to-lisp node state having (1+ indent)))
  (if (nref node order-by)
      (pgqa-node-slot-to-lisp node state order-by (1+ indent)))
  (if (nref node target-table)
      (pgqa-node-slot-to-lisp node state target-table (1+ indent)))
  (if (nref node insert-cols)
      (pgqa-node-slot-to-lisp node state insert-cols (1+ indent)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-from-expr (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (if (field-boundp node 'from-list)
      (pgqa-list-slot-to-lisp node state from-list (1+ indent)))

  (if (field-boundp node 'qual)
      (pgqa-node-slot-to-lisp node state qual (1+ indent)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-from-list-entry (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (if (field-boundp node 'alias)
      (pgqa-node-slot-to-lisp node state alias (1+ indent)))

  (if (field-boundp node 'lateral)
      (pgqa-simple-slot-to-lisp node state lateral (1+ indent)))

  (if (field-boundp node 'kind)
      (pgqa-simple-slot-to-lisp node state kind (1+ indent)))

  (if (field-boundp node 'args)
      (pgqa-list-slot-to-lisp node state args (1+ indent)))

  (if (field-boundp node 'qual)
      (pgqa-node-slot-to-lisp node state qual (1+ indent)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-from-list-entry-alias (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (pgqa-node-slot-to-lisp node state name (1+ indent))
  (if (field-boundp node 'cols)
      (pgqa-node-slot-to-lisp node state cols (1+ indent)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-top-clause (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (if (field-boundp node 'expr)
      (pgqa-node-slot-to-lisp node state expr (1+ indent)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-func-call (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (pgqa-node-slot-to-lisp node state name (1+ indent))

  (when (and (field-boundp node 'args) (nref node args))
    (cl-assert
     (pgqa-object-of-class-p (nref node args) 'node-list)
     nil
     "Function arguments should have been wrapped in a comma  operator")

    (let ((comma-op (nref node args)))
      (pgqa-list-slot-to-lisp comma-op state args (1+ indent))))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-string (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (pgqa-dump-raw-append state
			(format "%s%s\n"
				(pgqa-lisp-indent-string (1+ indent))
				(nref node value)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-number (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (pgqa-dump-raw-append state
			(format "%s%s\n"
				(pgqa-lisp-indent-string (1+ indent))
				(nref node value)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-obj (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (pgqa-dump-raw-append state
			(format "%s%s\n"
				(pgqa-lisp-indent-string (1+ indent))
				(mapconcat 'format (nref node args) ".")))
  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-data-type (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (pgqa-list-slot-to-lisp node state args (1+ indent))
  (pgqa-simple-slot-to-lisp node state dims (1+ indent))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-case (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (if (nref node arg)
      (pgqa-node-slot-to-lisp node state arg (1+ indent)))
  (pgqa-list-slot-to-lisp node state branches (1+ indent))
  (if (field-boundp node 'else)
      (pgqa-node-slot-to-lisp node state else (1+ indent)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-case-branch (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)
  (pgqa-list-slot-to-lisp node state args (1+ indent))
  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-operator (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (pgqa-simple-slot-to-lisp node state op (1+ indent))
  (if (field-boundp node 'args)
      (pgqa-list-slot-to-lisp node state args (1+ indent)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-node-list (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (if (field-boundp node 'args)
      (pgqa-list-slot-to-lisp node state args (1+ indent)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-target-entry (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)

  (pgqa-node-slot-to-lisp node state expr (1+ indent))
  (if (field-boundp node 'alias)
      (pgqa-node-slot-to-lisp node state alias (1+ indent)))

  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-alias (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)
  (pgqa-simple-slot-to-lisp node state name (1+ indent))
  (pgqa-dump-raw-boundary node-end node state indent))

(defun pgqa-dump-raw-alias-arg (node state indent)
  (pgqa-dump-raw-boundary node-start node state indent)
  (pgqa-simple-slot-to-lisp node state var (1+ indent))
  (if (field-boundp node 'datatype)
      (pgqa-node-slot-to-lisp node state datatype (1+ indent)))
  (pgqa-dump-raw-boundary node-end node state indent))

(provide 'pgqa-dump-raw)
