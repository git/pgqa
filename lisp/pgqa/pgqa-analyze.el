;; Copyright (C) 2016N Antonin Houska
;;
;; This file is part of PGQA.
;;
;; PGQA is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; PGQA is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License qalong
;; with PGQA. If not, see <http://www.gnu.org/licenses/>.

(eval-when-compile
  (require 'pgqa-node-api))

;; Keep byte compiler quitet.
(defvar pgqa-class-field-name)
(defvar pgqa-class-field-parent)
(defvar pgqa-class-field-fields)
(defvar pgqa-class-field-defaults)
(defvar pgqa-class-field-methods)
(defvar pgqa-class-method-dump-raw)
(defvar pgqa-class-method-dump-node)
(defvar pgqa-class-method-walk)
(defvar pgqa-class-method-analyze)
(defvar pgqa-object-field-class)
(defvar pgqa-object-field-fields)
(defvar pgqa-log-buffer)

(defun pgqa-show-problems (problems buffer)
  "Show problems in the appropriate buffer."

  ;; Make sure that the output buffer exists.
  (setq pgqa-log-buffer (get-buffer-create pgqa-log-buffer))

  ;; Display the errors / warnings.
  (with-current-buffer pgqa-log-buffer
    (atomic-change-group
      (setq buffer-read-only nil)
      (erase-buffer)
      (if (car problems)
	  (progn
	    (let ((button)
		  (pos)
		  (i 0))
	      (dolist (p problems)
		;; Separate the message from the previous one.
		(if (> i 0)
		    (insert "\n"))

		(setq pos (point))
		(insert (format "%s\n" (nref p message)))

		(when (null noninteractive)
		  ;; Create a text button so that user can navigate to the
		  ;; related node by clicking on the problem text.
		  (setq button
			(make-text-button pos (1- (point))))

		  ;; Set the location of the problem is available to the
		  ;; action function(s).
		  (button-put button 'err-loc (nref p location))
		  (button-put button 'query-buffer buffer)
		  ;; Mouse click should also lead to the problem location.
		  (button-put button 'follow-link t)

		  (button-put button 'action
			      (lambda (button)
				(let* ((loc (button-get button 'err-loc))
				       (buf (button-get button
							'query-buffer)))
				  ;; Point at the problem location.
				  (with-current-buffer buf
				    (switch-to-buffer-other-window
				     (current-buffer))

				    (if (sequencep loc)
					;; Both start and end position is
					;; known.
					(progn
					  (goto-char (elt loc 0))
					  (push-mark)
					  ;; Mark the problem location.
					  (activate-mark)
					  (goto-char (elt loc 1))
					  ;; Wait a little bit before
					  ;; deactivating the mark.
					  (run-at-time ".5 sec" nil
						       (lambda ()
							 (goto-char (mark))
							 (pop-mark))))
				      ;; Only the start position is known.
				      (goto-char loc))
				    )
				  )
				)
			      )
		  )

		;; Get ready for the next message.
		(setq pos (point))
		(setq i (1+ i)))
	      )
	    ;; Let user see the buffer.
	    (switch-to-buffer-other-window (current-buffer)))
	(insert "No problems found"))
      ;; Mark the buffer read-only so that user does not mess it up when
      ;; hitting ENTER to navigate to the error locations.
      (setq buffer-read-only t))
    )

    (when (car problems)
      ;; Do not let the processing continue, for whatever reason the function
      ;; is called. This must happen outside atomic-change-group so that the
      ;; pgqa-log-buffer setup is not undone.
      ;;
      ;; Actually not all problems identified during the analysis break
      ;; query formatting, however if we proceeded, then the links to
      ;; the query text would become invalid.
      ;;
      ;; XXX In the batch mode, print out the contents of
      ;; pgqa-log-buffer to standard output.
      (user-error "%d problem(s) found in the query" (length problems)))
  )

;; The entry point to call pgqa-analyze-... object method.
(defun pgqa-analyze (node context)
  (let* ((class (aref node pgqa-object-field-class))
	 (methods (aref class pgqa-class-field-methods))
	 (method (aref methods pgqa-class-method-analyze)))
    (if method
	(funcall method node context)))
  )

(defun pgqa-analyze-from-list-entry (node context)
  (let ((out-list (nref context problems)))
    ;; Parser should currently enforce the alias, but the error message is not
    ;; too user friendly. So we might allow absence of the alias in the
    ;; grammar someday and the analyzer complain here.
    (when (string= (nref node kind) "query")
      (if (null (field-boundp node 'alias))
	  (push
	   (make-instance pgqa-problem
			  :message "Subquery must have an alias"
			  :location node)
	   out-list))
      (let ((alias (nref node alias)))
	(when (field-boundp alias 'cols)
	  (let* (
		 ;; The actual query is the first arg of the FROM list entry
		 ;; node.
		 (query (car (nref node args)))
		 (query-tlist (nref query target-expr))
		 (query-cols (nref query-tlist args))
		 (cols-tmp query-cols)
		 (has-asterisk))
	    ;; If the targetlist contanis an asterisk, we don't know how many
	    ;; output columns the query actually has.
	    (while (and (null has-asterisk) cols-tmp)
	      (let ((expr (nref (car cols-tmp) expr)))
		(when (eq (pgqa-object-class-name expr) 'obj)
		  (dolist (e (nref expr args))
		    (if (string= e "*")
			(setq has-asterisk t))
		    )
		  )
		)
	      (setq cols-tmp (cdr cols-tmp)))

	    (unless has-asterisk
	      (let* ((ncols-query (length query-cols))
		     (arg-list (nref alias cols))
                     (ncols-alias (length (nref arg-list args))))
		(if (>
		     ncols-alias
		     ncols-query)
		    (push
		     (make-instance
		      pgqa-problem
		      :message
		      (format
		       "Subquery has %d columns available but %d columns specified in the alias"
		       ncols-query ncols-alias)
		      :location (nref alias region))
		     out-list))
		)
	      )
	    )
	  )
	)
      )

    (if (and (nref node lateral)
	     (string= (nref node kind) "table"))
	(push
	 (make-instance
	  pgqa-problem
	  :message "Table cannot be prepended with LATERAL keyword"
	  :location (nref node region))
	 out-list))

    (nset context problems out-list)
    )
  )

(provide 'pgqa-analyze)
